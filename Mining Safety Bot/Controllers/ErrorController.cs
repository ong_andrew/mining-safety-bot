﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mining_Safety_Bot
{
    public class ErrorController : Controller
    {
        public ActionResult Index(string message, string innermessage)
        {
            ViewData["Message"] = message;

            return View();
        }
    }
}