﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Mining_Safety_Bot
{
    public class NotificationsController : ApiController
    {
        public IEnumerable<Notification> Get()
        {
            return new List<Notification>();
        }

        public Notification Get(string id)
        {
            return new Notification();
        }
        
        public HttpResponseMessage Post()
        {
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}
