﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Mining_Safety_Bot
{
    public class ExistingHazardsController : ApiController
    {
        public IEnumerable<ExistingHazards> Get()
        {
            return new List<ExistingHazards>();
        }

        public ExistingHazards Get(string id)
        {
            return new ExistingHazards();
        }

        public HttpResponseMessage Post()
        {
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}
