﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Mining_Safety_Bot
{
    public class PublicIncidentController : ApiController
    {
        public IEnumerable<PublicIncident> Get()
        {
            return new List<PublicIncident>();
        }

        public PublicIncident Get(string id)
        {
            return new PublicIncident();
        }

        public HttpResponseMessage Post()
        {
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}
