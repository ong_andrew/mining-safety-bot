﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Mining_Safety_Bot
{
    [Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewData["UserId"] = HttpUtility.UrlDecode(HttpContext.Request.Cookies["UPN"].Value);
            ViewData["UserName"] = HttpUtility.UrlDecode(HttpContext.Request.Cookies["GivenName"].Value);

            return View();
        }

        public ActionResult Chat()
        {
            ViewData["UserId"] = HttpUtility.UrlDecode(HttpContext.Request.Cookies["UPN"].Value);
            ViewData["UserName"] = HttpUtility.UrlDecode(HttpContext.Request.Cookies["GivenName"].Value);

            return View();
        }

        public ActionResult Mobile()
        {
            ViewData["UserId"] = HttpUtility.UrlDecode(HttpContext.Request.Cookies["UPN"].Value);
            ViewData["UserName"] = HttpUtility.UrlDecode(HttpContext.Request.Cookies["GivenName"].Value);

            return View();
        }

        public ActionResult Bot(string userid = "User", string username = "User")
        {
            ViewData["UserId"] = userid;
            ViewData["UserName"] = username;
            
            return View();
        }

        public async Task<ActionResult> Login(string state, string code)
        {
            return View();
        }
    }
}