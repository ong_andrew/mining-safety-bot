﻿using Microsoft.Owin;
using Owin;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OpenIdConnect;
using System.Configuration;
using System.IdentityModel.Tokens;
using System.Security.Claims;
using System.Net.Http;
using System.Net;
using Newtonsoft.Json;
using Microsoft.IdentityModel.Protocols;
using Microsoft.Bot.Connector;
using System;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Text;
using System.Data.SqlTypes;

[assembly: OwinStartup(typeof(Mining_Safety_Bot.Startup))]
namespace Mining_Safety_Bot
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }

        public void ConfigureAuth(IAppBuilder app)
        {
            app.SetDefaultSignInAsAuthenticationType(CookieAuthenticationDefaults.AuthenticationType);
            
            app.UseKentorOwinCookieSaver();

            app.UseCookieAuthentication(new CookieAuthenticationOptions());
            
            app.UseOpenIdConnectAuthentication(
                new OpenIdConnectAuthenticationOptions
                {
                    ClientId = ConfigurationManager.AppSettings["MicrosoftAppId"],
                    Authority = "https://login.microsoftonline.com/common/v2.0",
                    RedirectUri = ConfigurationManager.AppSettings["RedirectUri"],
                    Scope = "openid email profile offline_access User.Read",
                    PostLogoutRedirectUri = ConfigurationManager.AppSettings["RedirectUri"],
                    TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = false
                    },
                    Notifications = new OpenIdConnectAuthenticationNotifications
                    {
                        AuthorizationCodeReceived = async (context) =>
                        {
                            try
                            {
                                string[] scopes = { "User.Read" };
                                
                                using (HttpClient httpClient = new HttpClient())
                                {
                                    Dictionary<string, string> values = new Dictionary<string, string>
                                    {
                                        { "client_id", ConfigurationManager.AppSettings["MicrosoftAppId"] },
                                        { "scope", string.Join(" ", scopes) },
                                        { "code", $"{context.Code}" },
                                        { "redirect_uri", ConfigurationManager.AppSettings["RedirectUri"] },
                                        { "grant_type", "authorization_code" },
                                        { "client_secret", ConfigurationManager.AppSettings["MicrosoftAppPassword"] }
                                    };

                                    FormUrlEncodedContent formContent = new FormUrlEncodedContent(values);
                                    
                                    HttpResponseMessage httpResponseMessage = await httpClient.PostAsync("https://login.microsoftonline.com/common/oauth2/v2.0/token", formContent);

                                    string responseString = await httpResponseMessage.Content.ReadAsStringAsync();

                                    if (httpResponseMessage.StatusCode == HttpStatusCode.OK)
                                    {
                                        dynamic tokenPayload = JsonConvert.DeserializeObject(responseString);

                                        //JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
                                        //JwtSecurityToken jsonToken = (JwtSecurityToken)tokenHandler.ReadToken(tokenPayload.access_token.ToString());
                                        //List<Claim> claims = (List<Claim>)jsonToken.Claims;

                                        //string upn = claims.First(claim => claim.Type == "upn").Value;
                                        //string givenName = claims.First(claim => claim.Type == "given_name").Value;

                                        Dictionary<string, string> graphInformation = await GetUserInfoFromMicrosoftGraph(tokenPayload.access_token.ToString());

                                        string upn = graphInformation["UserPrincipalName"];
                                        string givenName = graphInformation["GivenName"];

                                        context.Response.Cookies.Append("UPN", upn);
                                        context.Response.Cookies.Append("GivenName", givenName);

                                        StateClient stateClient = new StateClient(new MicrosoftAppCredentials(ConfigurationManager.AppSettings["MicrosoftAppId"], ConfigurationManager.AppSettings["MicrosoftAppPassword"]));
                                        BotData botData = await stateClient.BotState.GetUserDataAsync("webchat", upn);

                                        botData.SetProperty<bool>("Authenticated", true);
                                        

                                        await WriteToDatabase(graphInformation);

                                        foreach (KeyValuePair<string, string> item in graphInformation)
                                        {
                                            botData.SetProperty<string>(item.Key, item.Value);
                                        }

                                        await stateClient.BotState.SetUserDataAsync("webchat", upn, botData);
                                    }
                                    else
                                    {
                                        throw new UnauthorizedAccessException();
                                    }
                                }
                            }
                            catch (Exception e)
                            {
                                context.Response.Redirect("/Error?message=" + e.Message);
                                //context.Response.Redirect(string.Format("https://{0}{1}", context.Request.Uri.Authority, context.Request.Uri.AbsolutePath));
                                context.HandleResponse();
                                return;
                            }
                        },
                        AuthenticationFailed = async (notification) =>
                        {
                            notification.HandleResponse();
                            notification.Response.Redirect("/Error?message=" + notification.Exception.Message);
                        },
                        RedirectToIdentityProvider = async (context) =>
                        {
                            if (context.ProtocolMessage.RequestType == OpenIdConnectRequestType.AuthenticationRequest)
                            {
                                if (!context.Request.IsSecure)
                                {
                                    context.Response.Redirect(string.Format("https://{0}{1}", context.Request.Uri.Authority, context.Request.Uri.AbsolutePath));
                                    context.HandleResponse();
                                }
                            }
                        }
                    }
                });
        }

        
        private async Task CheckPermit(Dictionary<string, string> graphInformation)
        {

            using (SqlConnection sqlConnection = Connections.GetSqlConnection())
            {
                sqlConnection.Open();
                StringBuilder stringBuilder = new StringBuilder();
                //stringBuilder.Append("SELECT [Location] FROM [WorkOrders] WHERE [UserPrincipalName] = '"+ graphInformation["UserPrincipalName"] +"' AND [CompletionDate] IS NULL))");
                stringBuilder.Append("SELECT [Location] FROM [WorkOrders] WHERE [CompletionDate] IS NULL))");

                using (SqlCommand sqlCommand = new SqlCommand(stringBuilder.ToString(), sqlConnection))
                {
                    SqlDataReader sqlDataReader = await sqlCommand.ExecuteReaderAsync();

                    stringBuilder.Clear();

                    while (await sqlDataReader.ReadAsync())
                    {
                        stringBuilder.Append($" - Following location is in a workorder {sqlDataReader.GetSqlString(0)} \n\n");
                    }
                }
            }
        }
        

        private async Task WriteToDatabase(Dictionary<string, string> graphInformation)
        {
            bool UserExists = false;
            using (SqlConnection sqlConnection = Connections.GetSqlConnection())
            {
                await sqlConnection.OpenAsync();
                using (SqlCommand sqlCommandIfExists = new SqlCommand("SELECT [UserPrincipalName] FROM [PersonnelDetails] WHERE [UserPrincipalName] = @UserPrincipalName", sqlConnection))
                {
                    sqlCommandIfExists.Parameters.AddWithValue("@UserPrincipalName", new SqlString(graphInformation["UserPrincipalName"]));

                    SqlDataReader sqlDataReader = await sqlCommandIfExists.ExecuteReaderAsync();

                    if (sqlDataReader.HasRows)
                    {
                        UserExists = true;

                    }
                }
            }

            if(!UserExists)
            {
                using (SqlConnection sqlConnection = Connections.GetSqlConnection())
                {
                    await sqlConnection.OpenAsync();
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.Append("INSERT INTO [PersonnelDetails] ([UserPrincipalName], [GivenName], [Surname], [JobTitle], [OfficeLocation], [Email]) ");
                    stringBuilder.Append("VALUES (@UserPrincipalName, @GivenName, @Surname, @JobTitle, @OfficeLocation, @Email);");

                    using (SqlCommand sqlCommand = new SqlCommand(stringBuilder.ToString(), sqlConnection))
                    {
                        sqlCommand.Parameters.AddWithValue("@UserPrincipalName", new SqlString(graphInformation["UserPrincipalName"]));
                        sqlCommand.Parameters.AddWithValue("@GivenName", new SqlString(graphInformation["GivenName"]));
                        sqlCommand.Parameters.AddWithValue("@Surname", new SqlString(graphInformation["Surname"]));
                        sqlCommand.Parameters.AddWithValue("@JobTitle", new SqlString(graphInformation["JobTitle"]));
                        sqlCommand.Parameters.AddWithValue("@OfficeLocation", new SqlString(graphInformation["OfficeLocation"]));
                        sqlCommand.Parameters.AddWithValue("@Email", new SqlString(graphInformation["Email"]));


                        await sqlCommand.ExecuteNonQueryAsync();
                    }
                }
            }

            using (SqlConnection sqlConnection = Connections.GetSqlConnection())
            {
                await sqlConnection.OpenAsync();
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append("UPDATE [PersonnelDetails] SET [LastLoginDate] = @LastLoginDate WHERE [UserPrincipalName] = @UserPrincipalName ");

                using (SqlCommand sqlCommand = new SqlCommand(stringBuilder.ToString(), sqlConnection))
                {

                    sqlCommand.Parameters.AddWithValue("@LastLoginDate", new SqlDateTime(DateTime.Now));
                    sqlCommand.Parameters.AddWithValue("@UserPrincipalName", new SqlString(graphInformation["UserPrincipalName"]));

                    await sqlCommand.ExecuteNonQueryAsync();
                }
            }
        }  

            /*
    using (SqlCommand sqlCommand = new SqlCommand("DELETE FROM [PersonnelDetails] WHERE [UserPrincipalName] = @UserPrincipalName", sqlConnection))
    {
        sqlCommand.Parameters.AddWithValue("@UserPrincipalName", new SqlString(graphInformation["UserPrincipalName"]));

        await sqlCommand.ExecuteNonQueryAsync();
    }
    */


                /*
            using (SqlConnection sqlConnection = Connections.GetSqlConnection())
            {
                await sqlConnection.OpenAsync();
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append("INSERT INTO [PersonnelDetails] ([UserPrincipalName], [GivenName], [Surname], [JobTitle], [OfficeLocation], [Email])");
                stringBuilder.Append("VALUES (@UserPrincipalName, @GivenName, @Surname, @JobTitle, @OfficeLocation, @Email);");

                using (SqlCommand sqlCommand = new SqlCommand(stringBuilder.ToString(), sqlConnection))
                {
                    sqlCommand.Parameters.AddWithValue("@UserPrincipalName", new SqlString(graphInformation["UserPrincipalName"]));
                    sqlCommand.Parameters.AddWithValue("@GivenName", new SqlString(graphInformation["GivenName"]));
                    sqlCommand.Parameters.AddWithValue("@Surname", new SqlString(graphInformation["Surname"]));
                    sqlCommand.Parameters.AddWithValue("@JobTitle", new SqlString(graphInformation["JobTitle"]));
                    sqlCommand.Parameters.AddWithValue("@OfficeLocation", new SqlString(graphInformation["OfficeLocation"]));
                    sqlCommand.Parameters.AddWithValue("@Email", new SqlString(graphInformation["Email"]));

                    await sqlCommand.ExecuteNonQueryAsync();
                }
            }
            */


        private async Task<Dictionary<string, string>> GetUserInfoFromMicrosoftGraph(string accessToken)
        {
            // Call the Microsoft Graph API.
            // This was written before the implementation of the GraphConnector in the project
            // and could be rewritten to use that.
            HttpClient httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("bearer", accessToken);
            HttpResponseMessage response = await httpClient.GetAsync("https://graph.microsoft.com/v1.0/me/");
            string responseBody = await response.Content.ReadAsStringAsync();

            dynamic json = JsonConvert.DeserializeObject(responseBody);

            // Retrieve some specific information and add them to a dictionary to be returned.
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            dictionary.Add("GivenName", json.givenName.ToString());
            dictionary.Add("MobilePhone", json.mobilePhone == null ? string.Empty : json.mobilePhone.ToString());
            dictionary.Add("OfficeLocation", json.officeLocation.ToString());
            dictionary.Add("JobTitle", json.jobTitle.ToString());
            dictionary.Add("Surname", json.surname.ToString());
            dictionary.Add("Email", json.mail.ToString());
            dictionary.Add("UserPrincipalName", json.userPrincipalName.ToString());
            //dictionary.Add("UserPrincipalName", json.mail.ToString());


            return dictionary;
        }
    }
}
