﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mining_Safety_Bot
{
    [Serializable]
    public class PublicIncident
    {
        public DateTime Occurence { get; set; }
        public string Detail { get; set; }
        public string ReferenceID { get; set; }
    }
}