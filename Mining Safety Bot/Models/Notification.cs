﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mining_Safety_Bot
{
    public enum NotificationType { Workload = 1, Breaktime, Health, Safety, Weather, Reminder, BroadcastAlert, SafetySuggestion };

    [Serializable]
    public class Notification
    {
        public int ID { get; set; }
        public DateTime Timestamp { get; set; }
        public string User { get; set; }
        public NotificationType Type { get; set; }
        public string Content { get; set; }
        public bool IsRead { get; set; }
    }
}