﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mining_Safety_Bot
{
    [Serializable]
    public class ExistingHazards
    {

        public DateTime Timestamp { get; set; }
        public string Detail { get; set; }

    }
}