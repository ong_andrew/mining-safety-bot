﻿using Microsoft.Bot.Builder.Dialogs.Internals;
using Microsoft.Bot.Builder.Internals.Fibers;
using Microsoft.Bot.Builder.Scorables.Internals;
using Microsoft.Bot.Connector;
using System.Threading;
using System.Threading.Tasks;

namespace Mining_Safety_Bot
{
    public class CommandScorable : ScorableBase<IActivity, IMessageActivity, double>
    {
        private readonly IDialogTask task;
        
        public CommandScorable(IDialogTask task)
        {
            SetField.NotNull(out this.task, nameof(task), task);
        }
        
        protected override async Task<IMessageActivity> PrepareAsync(IActivity activity, CancellationToken token)
        {
            IMessageActivity message = (IMessageActivity)activity;

            if (message.Text == null)
            {
                return null;
            }
            else if (message.Text.ToLowerInvariant().StartsWith("!"))
            {
                return message;
            }
            else if(message.Text.ToLowerInvariant().StartsWith("home"))
            {
                return message;
            }
            else
            {
                return null;
            }
        }
        
        protected override bool HasScore(IActivity item, IMessageActivity message)
        {
            return message != null;
        }
        
        protected override double GetScore(IActivity item, IMessageActivity message)
        {
            return 1.0;
        }
        
        protected override async Task PostAsync(IActivity item, IMessageActivity message, CancellationToken token)
        {
            if (message.Text.ToLowerInvariant().StartsWith("home"))
            {
                this.task.Reset();

                message.Value = "show menu";

                await this.task.InterruptAsync(new RootDialog(), message, token);
            }
            else
            {
                await this.task.InterruptAsync(new CommandDialog(), message, token);
            }
        }
        
        protected override Task DoneAsync(IActivity item, IMessageActivity state, CancellationToken token)
        {
            return Task.CompletedTask;
        }
    }
}