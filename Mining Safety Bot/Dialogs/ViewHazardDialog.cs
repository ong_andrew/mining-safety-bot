﻿using System;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using Microsoft.Bot.Builder.Luis;
using Microsoft.Bot.Builder.Luis.Models;
using System.Threading;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlTypes;
using AdaptiveCards;
using System.Text.RegularExpressions;
using System.Data;

namespace Mining_Safety_Bot
{
    [Serializable]
    public class ViewHazardDialog : IDialog<IMessageActivity>
    {
        public async Task StartAsync(IDialogContext context)
        {
            context.Wait(MessageReceivedAsync);
        }

        
        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            IMessageActivity message = await result;

            dynamic value = message.Value;

            switch (value.FormType.ToString())
            {
                case "AddDetails":

                    if (value.Item0 != null)
                    {
                        AdaptiveCard card = new AdaptiveCard();
                        card.Body.Add(new TextBlock()
                        {
                            Text = "Please make your additions below",
                            Weight = TextWeight.Bolder,
                            Size = TextSize.Medium
                        });

                        card.Body.Add(new TextBlock()
                        {
                            Text = value.Item0,
                            Wrap = true
                        });

                        card.Body.Add(new TextBlock()
                        {
                            Text = "Detail",
                        });

                        card.Body.Add(new TextInput()
                        {
                            Id = "Detail",
                            IsMultiline = true,
                            IsRequired = true
                        });

                        card.Actions.Add(new SubmitAction()
                        {
                            Title = "Submit Additions",
                            DataJson = JObject.Parse("{ \"FormSubject\": \"HazardUpdate\", \"FormType\": \"HazardUpdate\" }").ToString()
                        });
                        Attachment resourceCardAttachment = new Attachment()
                        {
                            ContentType = AdaptiveCard.ContentType,
                            Content = card
                        };

                        IMessageActivity resourceFormMessage = context.MakeMessage();
                        resourceFormMessage.Attachments = new List<Attachment>();
                        resourceFormMessage.Attachments.Add(resourceCardAttachment);

                        await context.PostAsync(resourceFormMessage);
                    }
                    if (value.Item1 != null)
                    {
                        AdaptiveCard card = new AdaptiveCard();
                        card.Body.Add(new TextBlock()
                        {
                            Text = "Please make your additions below",
                            Weight = TextWeight.Bolder,
                            Size = TextSize.Medium
                        });

                        card.Body.Add(new TextBlock()
                        {
                            Text = value.Item1,
                            Wrap = true
                        });

                        card.Body.Add(new TextBlock()
                        {
                            Text = "Location",
                            Separation = SeparationStyle.Strong
                        });

                        card.Body.Add(new TextInput()
                        {
                            Id = "Location",
                            IsMultiline = false,
                            IsRequired = true
                        });

                        card.Body.Add(new TextBlock()
                        {
                            Text = "Detail",
                        });

                        card.Body.Add(new TextInput()
                        {
                            Id = "Detail",
                            IsMultiline = true,
                            IsRequired = true
                        });

                        card.Actions.Add(new SubmitAction()
                        {
                            Title = "Submit Additions",
                            DataJson = JObject.Parse("{ \"FormSubject\": \"HazardUpdate\", \"FormType\": \"HazardUpdate\" }").ToString()
                        });
                        Attachment resourceCardAttachment = new Attachment()
                        {
                            ContentType = AdaptiveCard.ContentType,
                            Content = card
                        };

                        IMessageActivity resourceFormMessage = context.MakeMessage();
                        resourceFormMessage.Attachments = new List<Attachment>();
                        resourceFormMessage.Attachments.Add(resourceCardAttachment);

                        await context.PostAsync(resourceFormMessage);
                    }

                    break;

                case "MoreDetails":

                    if (value.Item0 != null)
                    {
                        if (value.Item1 != null)
                        {
                            await context.PostAsync("Let me get the details for those 2 hazards for you now...");
                        }
                        else
                        {
                            await context.PostAsync("Let me get the details of that hazard for you now...");
                        }
                    }
                    else if (value.Item1 != null)
                    {
                        await context.PostAsync("Let me get the details of that hazard for you now...");
                    }
                    break;
                case "EmailtoMe":
                    {
                        /*
                        int i = 0;
                        dynamic var = "Item0";
                        StringBuilder stringBuilder = new StringBuilder();
                        await context.PostAsync(value.Item0);
                        await context.PostAsync(value.Item0);
                        await context.PostAsync(value.var);
                        while (value.var.ToString() != null)
                        {

                            await context.PostAsync(i.ToString());
                            stringBuilder.Append(value.var+"\n\n");
                            i = i+1;
                            var = "Item" + i.ToString();
                        }
                        await context.PostAsync(stringBuilder.ToString());
                        await context.PostAsync("I have sent those details to your email.");
                        */
                        if (value.Item0 != null)
                        {
                            await context.PostAsync("I'll send those details through for you now.");
                            await Connections.SendHazardDetails(context.UserData.GetValueOrDefault("UserPrincipalName", string.Empty), value.Item0.ToString());
                            await context.PostAsync("What would you like to do next?");
                        }
                        else
                        {
                            await context.PostAsync("Please enter the report that you would like emailed.");
                        }
                    }
                        break;
                case "SharetoSupervisor":
                    await context.PostAsync("I have emailed the details of these hazards to your supervisor and created a notification on his phone.");
                    await context.PostAsync("What would you like to do next?");
                    break;                    

                default:
                    // Not sure what form we received. Feedback.
                    await context.PostAsync("Unknown option received.");
                    break;

            }

            // Whatever the outcome, end the dialog.
            //context.Done<IMessageActivity>(null);
        }
    }
}