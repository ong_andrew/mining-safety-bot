﻿using System;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using Microsoft.Bot.Builder.Luis;
using Microsoft.Bot.Builder.Luis.Models;
using System.Threading;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlTypes;
using AdaptiveCards;
using System.Text.RegularExpressions;
using System.Data;

namespace Mining_Safety_Bot
{
    [Serializable]
    public class PpeDialog : IDialog<IMessageActivity>
    {
        public async Task StartAsync(IDialogContext context)
        {
            context.Wait(MessageReceivedAsync);
        }

        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            IMessageActivity message = await result;

            dynamic value = message.Value;

            switch (value.FormType.ToString())
            {
                case "PPEOrderForm":
                    HashSet<string> ppe = new HashSet<string>();
                    string location = value.Locations;

                    //if (value.ppe == null)
                    //{
                    //    await context.PostAsync("Great, you have all the required PPE. What would you like to do next?");
                    // }
                    //else
                    if (value.Item == null)
                    {
                        await context.PostAsync("Your order form is empty");
                        break;
                    }
                    else if (value.Locations == null)
                    {
                        await context.PostAsync("No pickup location has been selected");
                        break;
                    }
                    else
                    {
                        foreach (string aPPE in value.Item.ToString().Split(';'))
                        {
                            ppe.Add(aPPE);
                        }
                    }

                    if (value.Locations == null)
                    {
                        await context.PostAsync("You did not enter a pickup location.");
                    }

                    await PPEOrder(context, ppe, location);
                    break;
                case "PPENoneRequired":
                    await context.PostAsync("Great. What would you like to do next?");
                    break;

                default:
                    // Not sure what form we received. Feedback.
                    await context.PostAsync("Unknown resourcing form received.");
                    break;

            }

            // Whatever the outcome, end the dialog.
            //context.Done<IMessageActivity>(null);
        }


        private async Task PPEOrder(IDialogContext context, HashSet<string> ppe, string location)
        {
            IMessageActivity reply = context.MakeMessage();


            bool hasPPE = ppe.Count != 0;
            bool hasLocation = location != "";
            await Task.Delay(1500);

            if (location == "East Site, Vending Machine 2")
            {
                await context.PostAsync("Your order is ready to be picked up from Vending Machine 2. When you get to the machine, swipe your token and your " +
                    "equipment will automatically be dispensed.");
            }
            else if (location == "North Site, Supply Centre")
            {
                await context.PostAsync("Your order will be ready for collection from the Supply Centre in 10 minutes");
            }
            else if (location == "East Site, Supply Centre")
            {
                await context.PostAsync("Your order will be ready for collection from the Supply Centre in 10 minutes");
            }
            await Task.Delay(1500);
            await context.PostAsync("What would you like to do next?");

        }
    }
}