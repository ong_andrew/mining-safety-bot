﻿using System;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using System.Threading;
using System.Collections.Generic;

namespace Mining_Safety_Bot
{
    [Serializable]
    public class RootDialog : IDialog<object>
    {
        public async Task StartAsync(IDialogContext context)
        {
            context.Wait(MessageReceivedAsync);

            return;
        }

        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> activity)

        {
            try
            {
                IMessageActivity message = await activity;

                await context.PostAsync("you did it");

                if (message.Value != null && message.Value.GetType() == typeof(string) && (string)message.Value == "show menu")
                {
                    await context.PostAsync(Messages.Menu(context));
                    context.Wait(MessageReceivedAsync);
                }
                else
                {
                    if (false)
                    //if (context.UserData.GetValueOrDefault("Authenticated", false) == false)
                    {
                        await context.PostAsync("You are not authenticated.");
                        context.Wait(MessageReceivedAsync);
                    }
                    else
                    {
                        await context.Forward(new IntentDialog(), ResumeAfterIntentDialog, message, CancellationToken.None);
                    }
                }
            }
            catch (Exception e)
            {
                context.PostAsync(e.ToString());
            }
        }

        private async Task ResumeAfterIntentDialog(IDialogContext context, IAwaitable<IMessageActivity> activity)
        {
            IMessageActivity message = await activity;

            if (message == null)
            {
                context.Wait(MessageReceivedAsync);
            }
            else
            {
               // await context.PostAsync("I don't understand that yet");

                HeroCard heroCard = new HeroCard();
                heroCard.Buttons = new List<CardAction>() {
                new CardAction() { Title = "Suggest a Feature", Type = "imBack", Value = "Suggest a feature" },
                new CardAction() { Title = "Return to Main Menu", Type = "imBack", Value = "Home" } };
                IMessageActivity reply = context.MakeMessage();


                reply.Text = "I don't understand that yet, but if you would like me to, make a Suggestion";

                reply.Attachments = new List<Attachment>() { heroCard.ToAttachment() };
                await context.PostAsync(reply);

            context.Wait(MessageReceivedAsync);
            }
        }
    }
}