﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Dialogs.Internals;
using Microsoft.Bot.Builder.Luis;
using Microsoft.Bot.Builder.Luis.Models;
using Microsoft.Bot.Connector;
using System;
using System.Threading.Tasks;
using System.Configuration;
using Universal.Microsoft.CognitiveServices;
using System.Text;
using Microsoft.Bot.Builder.FormFlow;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Newtonsoft.Json.Linq;
using AdaptiveCards;

namespace Mining_Safety_Bot
{
    [Serializable]
    [LuisModel("d8b80f93-13c1-45a6-8326-4b5951303a45", "7bc5ea92d17748648a56a637924d8ce2")]
    public class IntentDialog : LuisDialog<IMessageActivity>
    {
        private static string entityReportType = "ReportType";
        private static string entityHazard = "Hazard";
        private static string entityLocation = "Location";
        private static string entityNumber = "PermitNumber";
        private static string entityWorkOrderNumber = "WorkOrder";
        private static string entityJobType = "JobType";
        private static string entityToggle = "Toggle";
        private static string entityClassTime = "builtin.datetimeV2.datetime";
        private static string entityIncidentDetail = "JHATitle";
        private static string entityQuerySam = "QuerySam";
        private static string entityNotificationType = "NotificationType";
        private static string entityReferenceID = "IncidentReferenceID";
        private static string entityBuiltinNumber = "builtin.number";

        private static LuisApplication mLuisApplication;

        static IntentDialog()
        {
            //mLuisApplication = new LuisApplication(ConfigurationManager.AppSettings["d8b80f93-13c1-45a6-8326-4b5951303a45"], ConfigurationManager.AppSettings["e8d265b6cbde4cf18941ea7cbb916161"]);
            mLuisApplication = new LuisApplication(ConfigurationManager.AppSettings["LUIS:AppId"], ConfigurationManager.AppSettings["LUIS:ApiKey"]);
            mLuisApplication.Initialize();
        }
        [LuisIntent("MakeSuggestion")]
        public async Task MakeSuggestion(IDialogContext context, IAwaitable<IMessageActivity> activity, LuisResult result)
        {
            IMessageActivity message = await activity;
            dynamic value = message.Value;

            SuggestionForm suggestionForm = new SuggestionForm();
            await context.Forward(new FormDialog<SuggestionForm>(suggestionForm, SuggestionForm.BuildForm), ResumeAfterSuggestion, message);
            return;
        }

        private async Task ResumeAfterSuggestion(IDialogContext context, IAwaitable<SuggestionForm> awaitable)
        {
            SuggestionForm suggestionForm = await awaitable;

            try
            {
                using (SqlConnection sqlConnection = Connections.GetSqlConnection())
                {
                    sqlConnection.Open();
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.Append("INSERT INTO [Suggestion] ([Suggestion])");
                    stringBuilder.Append("VALUES (@Suggestion);");

                    using (SqlCommand sqlCommand = new SqlCommand(stringBuilder.ToString(), sqlConnection))
                    {
                        sqlCommand.Parameters.AddWithValue("@Suggestion", new SqlString(suggestionForm.SuggestionDetail));

                        await sqlCommand.ExecuteNonQueryAsync();
                    }

                }
                await context.PostAsync("Suggestion Submitted. Thank you.");
            }
            catch (Exception e)
            {

                await context.PostAsync(e.Message);
                await context.PostAsync("There was an error submitting your Suggestion. Please try again later.");
            }

            await context.PostAsync(Messages.Menu(context));
            context.Done<IMessageActivity>(null);
        }


        [LuisIntent("")]
        [LuisIntent("None")]
        public async Task None(IDialogContext context, IAwaitable<IMessageActivity> activity, LuisResult result)
        {
            IMessageActivity message = await activity;

            context.Done<IMessageActivity>(message);
        }

        private async Task NameDialogAfter(IDialogContext context, IAwaitable<IMessageActivity> activity)
        {
            context.Done<IMessageActivity>(null);
        }

        [LuisIntent("Greeting")]
        public async Task Greeting(IDialogContext context, IAwaitable<IMessageActivity> activity, LuisResult result)
        {
            IMessageActivity message = await activity;

            BotData userData = await message.GetStateClient().BotState.GetUserDataAsync(message.ChannelId, message.From.Id);

            if (userData.GetProperty<string>("PreferredName") == null)
            {
                await context.PostAsync("Hello to you too!");
                if (message.From.Name.ToLowerInvariant() != "you" && message.From.Name.ToLowerInvariant() != "user")
                {
                    await context.PostAsync($"I'll call you {message.From.Name} from now on. Tell me if you would like to change it.");
                    context.UserData.SetValue<string>("PreferredName", message.From.Name);
                    context.Done<IMessageActivity>(null);
                }
                else
                {
                    await context.PostAsync("What would you like me to call you?");
                    context.Call(new NameDialog(), this.NameDialogAfter);
                    await context.PostAsync(Messages.Menu(context));
                }
            }
            else
            {
                await context.PostAsync($"Hello {userData.GetProperty<string>("PreferredName")}!");
                context.Done<IMessageActivity>(null);
            }
        }
        
        private static LocationType GetLocationType(string aLocation)
        {
            if (aLocation.ToLowerInvariant() == "crusher 3" || aLocation.ToLowerInvariant() == "crusher3")
            {
                return LocationType.Crusher3;
            }
            else if (aLocation.ToLowerInvariant() == "maintenance workshop 1" || aLocation.ToLowerInvariant() == "maintenanceworkshop1")
            {
                return LocationType.MaintenanceWorkshop1;
            }
            else if (aLocation.ToLowerInvariant() == "train loadout 3"|| aLocation.ToLowerInvariant() == "trainloadout3")
            {
                return LocationType.TrainLoadout3;
            }
            else if (aLocation.ToLowerInvariant() == "car dumper 2"|| aLocation.ToLowerInvariant() == "cardumper2")
            {
                return LocationType.CarDumper2;
            }
            else if (aLocation.ToLowerInvariant() == "screening plant 1"|| aLocation.ToLowerInvariant() == "screeningplant1")
            {
                return LocationType.ScreeningPlant1;

            }
            else if (aLocation.ToLowerInvariant() == "hv workshop 2"|| aLocation.ToLowerInvariant() == "hvworkshop2")
            {
                return LocationType.HVWorkshop2;
            }
            else
            {
                
                return LocationType.Other;
            }
        }

        /*
        private static HazardType GetHazardType(string aHazard)
        {
            if (aHazard == "fire")
            {
                return HazardType.Fire;
            }
            else if (aHazard == "spill")
            {
                return HazardType.Spill;
            }
            else if (aHazard == "exposed cable")
            {
                return HazardType.ExposedCable;
            }
            else if (aHazard == "loud noise")
            {
                return HazardType.LoudNoise;
            }
            else
            {
                return HazardType.Other;
            }
        }
        */
        [LuisIntent("ViewExistingHazardReport")]
        public async Task ViewHazardReport(IDialogContext context, IAwaitable<IMessageActivity> activity, LuisResult result)
        {
            IMessageActivity message = await activity;
            dynamic value = message.Value;

            EntityRecommendation recommendationHazard;
            EntityRecommendation recommendationLocation;
            EntityRecommendation recommendationWorkOrderNumber;

            bool hashazard = result.TryFindEntity(entityHazard, out recommendationHazard);
            bool haslocation = result.TryFindEntity(entityLocation, out recommendationLocation);
            bool hasWorkOrderNumber = result.TryFindEntity(entityWorkOrderNumber, out recommendationWorkOrderNumber);

            HazardRequestForm hazardRequestForm = new HazardRequestForm();
            if (hasWorkOrderNumber)
            {
                string location = await Connections.GetWorkOrderLocationAsync(recommendationWorkOrderNumber.Entity);
                hazardRequestForm.Location = GetLocationType(location);
            }
            /*
            if (hashazard)
            {

                string reportHazard = mLuisApplication.GetCanonicalForm(entityHazard, recommendationHazard.Entity);

                hazardRequestForm.HazardType = GetHazardType(reportHazard);
            }
            */
            if (haslocation)
            {
                string reportLocation = mLuisApplication.GetCanonicalForm(entityLocation, recommendationLocation.Entity);
                hazardRequestForm.Location = GetLocationType(reportLocation);

                //hazardRequestForm.Location = GetLocationType(recommendationLocation.Entity);

            }
            await context.Forward(new FormDialog<HazardRequestForm>(hazardRequestForm, HazardRequestForm.BuildForm), ResumeAfterHazardRequest, message);

            return;
        }

        private async Task ResumeAfterHazardRequest(IDialogContext context, IAwaitable<HazardRequestForm> awaitable)
        {

            HazardRequestForm hazardRequestForm = await awaitable;
            string location = hazardRequestForm.Location.ToString();
  
            List<ExistingHazards> existingHazards = await Connections.CheckExistingHazards(location);

            if (existingHazards.Count == 0)
            {
                await context.PostAsync($"There are no Hazard Reports in {hazardRequestForm.Location.ToString()}\n\n");
            }
            else
            {

                AdaptiveCard ppeCard = new AdaptiveCard();

                ppeCard.Body.Add(new TextBlock()
                {
                    Text = $"Current Hazards",
                    Wrap = true,
                    Size = TextSize.Large,
                    Weight = TextWeight.Bolder

                });

                ppeCard.Body.Add(new TextBlock()
                {
                    Text = $"Please be aware of the following reported hazards at {Connections.ReformatString(location).Result} that have not yet been resolved:",
                    Wrap = true

                });

                List<AdaptiveCards.Choice> choices = new List<AdaptiveCards.Choice>();

                foreach (ExistingHazards hazard in existingHazards)
                { int i = 0;

                    choices.Add(new AdaptiveCards.Choice() { Title = hazard.Timestamp.ToLocalTime().ToString(), Value = hazard.Detail });
                    ppeCard.Body.Add(new ChoiceSet()
                    {
                        Id = "Item"+ i.ToString(),
                        Style = ChoiceInputStyle.Expanded,
                        IsMultiSelect = true,
                        IsRequired = true,
                        Choices = choices.Take(1).ToList(),
                        Separation = SeparationStyle.Strong
                    });

                    ppeCard.Body.Add(new TextBlock()
                    {
                        Text = hazard.Detail,
                        Wrap = true,

                    });
                    choices.Clear();
                    i = i + 1;

                }
                
                AdaptiveCard newCard = new AdaptiveCard();

                newCard.Actions.Add(new SubmitAction()
                {
                    Title = "Add Details",
                    DataJson = JObject.Parse("{ \"FormSubject\": \"HazardQuery\", \"FormType\": \"AddDetails\" }").ToString()
                });
                /*
                newCard.Actions.Add(new SubmitAction()
                {
                    Title = "More Details",
                    DataJson = JObject.Parse("{ \"FormSubject\": \"HazardQuery\", \"FormType\": \"MoreDetails\" }").ToString()
                });
                */
                newCard.Actions.Add(new SubmitAction()
                {
                    Title = "Email to Me",
                    DataJson = JObject.Parse("{ \"FormSubject\": \"HazardQuery\", \"FormType\": \"EmailtoMe\" }").ToString()
                });
                newCard.Actions.Add(new SubmitAction()
                {
                    Title = "Share to Supervisor",
                    DataJson = JObject.Parse("{ \"FormSubject\": \"HazardQuery\", \"FormType\": \"SharetoSupervisor\" }").ToString()
                });

                ppeCard.Actions.Add(new ShowCardAction()
                {
                    Title = "View Options",
                    Card = newCard
                });

                Attachment resourceCardAttachment = new Attachment()
                {
                    ContentType = AdaptiveCard.ContentType,
                    Content = ppeCard
                };

                IMessageActivity resourceFormMessage = context.MakeMessage();
                resourceFormMessage.Attachments = new List<Attachment>();
                resourceFormMessage.Attachments.Add(resourceCardAttachment);

                await context.PostAsync(resourceFormMessage);
                //context.Done<IMessageActivity>(null);
                //await context.PostAsync($"The following Hazard Reports are current to {Connections.ReformatString(hazardRequestForm.Location.ToString()).Result}:\n\n" + stringBuilder.ToString());
            }
            await Task.Delay(1500);
            await context.PostAsync("What would you like to do next?");
            
            /*
            try
            {

                
                using (SqlConnection sqlConnection = Connections.GetSqlConnection())
                {
                    sqlConnection.Open();
                    StringBuilder stringBuilder = new StringBuilder();
                    
                    stringBuilder.Append("SELECT [HazardType], [HazardDetail], [HazardLocation], [DateTimeofReport] FROM [HazardReports]" +
                        "WHERE [ResolvedDate] IS NULL AND [HazardLocation] = '" + hazardRequestForm.Location.ToString() + "'" +
                        "OR [ResolvedDate] IS NULL AND[HazardType] = '" + hazardRequestForm.HazardType.ToString() + "' ORDER BY[DateTimeofReport] DESC");
                        
                        
                    stringBuilder.Append("SELECT [HazardType], [HazardDetail], [HazardLocation], [DateTimeofReport] FROM [HazardReports]" +
                    "WHERE [ResolvedDate] IS NULL AND [HazardLocation] = '" + hazardRequestForm.Location.ToString()+"' ORDER BY[DateTimeofReport] DESC");

                    using (SqlCommand sqlCommand = new SqlCommand(stringBuilder.ToString(), sqlConnection))
                    {
                        SqlDataReader sqlDataReader = await sqlCommand.ExecuteReaderAsync();

                        stringBuilder.Clear();

                        while (await sqlDataReader.ReadAsync())
                        {
                            ExistingHazards existingHazards = new ExistingHazards()
                            {
                                Timestamp = sqlDataReader.GetSqlDateTime(3).Value,
                                Detail = sqlDataReader.GetSqlString(1).Value,

                            };

                            while (await sqlDataReader.ReadAsync())
                            {
                                //stringBuilder.Append($" - There is a report of a {sqlDataReader.GetSqlString(0)} in the {sqlDataReader.GetSqlString(2)} on the {sqlDataReader.GetSqlDateTime(3).ToString()} \n\n");
                                stringBuilder.Append($"* {sqlDataReader.GetSqlDateTime(3).ToString()}: {sqlDataReader.GetSqlString(1)}\n\n");

                            }
                            */

            context.Done<IMessageActivity>(null);
        }

        [LuisIntent("BookSafetyCourse")]
        public async Task BookSafetyCourse(IDialogContext context, IAwaitable<IMessageActivity> activity, LuisResult result)
        {
            IMessageActivity message = await activity;
            dynamic value = message.Value;

            EntityRecommendation recommendationPermitnumber;
            EntityRecommendation recommendationClassDateTime;
            bool hasPermitnumber = result.TryFindEntity(entityNumber, out recommendationPermitnumber);
            bool hasClassTime = result.TryFindEntity(entityClassTime, out recommendationClassDateTime);

            if (hasClassTime && hasPermitnumber)
            {
                string jsonAsStr = recommendationClassDateTime.Resolution["values"].ToString();

                JObject jsonObj = JObject.Parse(jsonAsStr.Substring(1, jsonAsStr.Length - 2));

                DateTime aDateTime = new DateTime();
                aDateTime = DateTime.Parse(jsonObj.GetValue("value").ToString());

                //await Connections.SendInvite(context.UserData.GetValueOrDefault("UserPrincipalName", string.Empty), aDateTime, recommendationPermitnumber.Entity);
                await Connections.SendInvite(context.UserData.GetValueOrDefault("UserPrincipalName", string.Empty), aDateTime, recommendationPermitnumber.Entity);
                await context.PostAsync("Great, your refresher session is booked. You will shortly recieve your calendar invite containing the relevant details.");
                await context.PostAsync(Messages.Menu(context));
                context.Done<IMessageActivity>(null);
                return;

            }
            else if (hasPermitnumber)
            {
                var dateAndTime = DateTime.Now;
                var date = dateAndTime.Date;

                IMessageActivity reply = context.MakeMessage();
                List<string> classtimes = new List<string>() { $"{date.AddDays(1).ToString("dd MMM")} at 12:30 pm", $"{date.AddDays(3).ToString("dd MMM")} at 1:30 pm", $" {date.AddDays(7).ToString("dd MMM")} at 9:00 am" };
                List<string> classvalues = new List<string>() { $"Please book me into the class on {date.AddDays(1).ToString("dd MMM yyyy")} at 12:30 pm to refresh Competency {recommendationPermitnumber.Entity}", $"Please book me into the class on {date.AddDays(3).ToString("dd MMM yyyy")} at 12:30 pm to refresh Competency {recommendationPermitnumber.Entity}", $"Please book me into the class on {date.AddDays(7).ToString("dd MMM yyyy")} at 12:30 pm to refresh Competency {recommendationPermitnumber.Entity}" };

                reply.Text = $"I have found the following classes to renew your Competency {recommendationPermitnumber.Entity}";

                HeroCard heroCard = new HeroCard();
                heroCard.Buttons = new List<CardAction>() {

                new CardAction() { Type = "imBack", Title = classtimes[0], Value = classvalues[0]},
                new CardAction() { Type = "imBack", Title = classtimes[1], Value = classvalues[1]},
                new CardAction() { Type = "imBack", Title = classtimes[2], Value = classvalues[2]},
                new CardAction() { Type = "imBack", Title = "Back to Home", Value = "Home" }};

                reply.Attachments = new List<Attachment>() { heroCard.ToAttachment() };

                await context.PostAsync(reply);
                context.Done<IMessageActivity>(null);
                return;
            }
            else if (!hasPermitnumber)
            {

                IMessageActivity reply = context.MakeMessage();
                string permitsExpiring = "None";
                permitsExpiring = await Connections.GetAllExpiringPermits();

                if (permitsExpiring != "None")
                {

                    reply.Text = $"Here is a list of your expiring Competencies:";
                    HeroCard heroCard = new HeroCard();
                    heroCard.Buttons = new List<CardAction>() { 
                    
                        new CardAction() { Type = "imBack", Title = permitsExpiring, Value = $"Book a refresher course for Competency {permitsExpiring}"}};
                    
                    reply.Attachments = new List<Attachment>() { heroCard.ToAttachment() };

                    await context.PostAsync(reply);

                }
                context.Done<IMessageActivity>(null);
                return;
            }
        }

        private async Task ResumeAfterPermitClassDialog(IDialogContext context, IAwaitable<string> awaitable)
        {
            string response = await awaitable;

            if (response.ToLowerInvariant() == "Home")
            {
                await context.PostAsync("I have not booked your class.");


            }
           
            else             
            {

                await context.PostAsync("Great, your refresher session is booked. A calendar invite will be sent to you shortly");

                try
                {
                    await context.PostAsync("ResumeAfterPermitClassDialog");
                    //await Connections.SendInvite(context.UserData.GetValueOrDefault("UserPrincipalName", string.Empty));
                }
                catch (Exception e)
                {
                    await context.PostAsync(e.Message);
                }

            }
            await context.PostAsync(Messages.Menu(context));
            context.Done<IMessageActivity>(null);
        }


        [LuisIntent("SafetyRecommendation")]
        public async Task SafetyInteraction(IDialogContext context, IAwaitable<IMessageActivity> activity, LuisResult result)
        {
            IMessageActivity message = await activity;
            dynamic value = message.Value;

            EntityRecommendation recommendationWorkOrderNumber;

            bool hasWorkOrderNumber = result.TryFindEntity(entityWorkOrderNumber, out recommendationWorkOrderNumber);
            
            IMessageActivity reply = context.MakeMessage(); 
            if (!hasWorkOrderNumber)
            {
                List<string> workOrders = new List<string>() { "W29330", "W30229", "W30340", "Return to Home" };
                List<string> workOrdersvalue = new List<string>() { "W29330", "W30229", "W30340", "Home" };
            
            //if (workOrders.Count != 0)
            
                reply.Text = "I can see that you have the following three work orders assigned to you today. Which would you like a Safety Briefing for?";

                HeroCard heroCard = new HeroCard();
                heroCard.Buttons = new List<CardAction>() { 

                new CardAction() { Type = "imBack", Title = workOrders[0], Value = workOrdersvalue[0] },
                new CardAction() { Type = "imBack", Title = workOrders[1], Value = workOrdersvalue[1] },
                new CardAction() { Type = "imBack", Title = workOrders[2], Value = workOrdersvalue[2] } ,
                new CardAction() { Type = "imBack", Title = workOrders[3], Value = workOrdersvalue[3] }};

            reply.Attachments = new List<Attachment>() { heroCard.ToAttachment() };
                await context.PostAsync(reply);
                context.UserData.RemoveValue($"Work Order W29330");
                context.UserData.RemoveValue($"Work Order W30229");
                context.UserData.RemoveValue($"Work Order W30340");
            }
            else
            {
                await ProcessWorkOrderQuery(context, recommendationWorkOrderNumber.Entity);
                return;
            }

            
            context.Call(new WorkOrderDialog(), ResumeAfterWorkOrderDialog);
        }

        private async Task ResumeAfterWorkOrderDialog(IDialogContext context, IAwaitable<string> awaitable)
        {
            try
            {
                string workOrderNumber = await awaitable;

                await ProcessWorkOrderQuery(context, workOrderNumber);
            }
            catch (Exception e)
            {
                await context.PostAsync(e.Message);
            }

        }

        public async Task ProcessWorkOrderQuery(IDialogContext context, string workOrderNumber)
        {
            string location = await Connections.GetWorkOrderLocationAsync(workOrderNumber);
            string detail = await Connections.GetWorkOrderDetailAsync(workOrderNumber);
            if (location == null || location == string.Empty)
            {
                await context.PostAsync("Sorry, I couldn't find details on that work order.");

                context.Done<IMessageActivity>(null);
                return;
            }
            else
            {
                IMessageActivity reply = context.MakeMessage();
                bool firstTime = context.UserData.TryGetValue<string>($"Work Order {workOrderNumber}", out string details);
                if (!firstTime)
                {
                    if (detail[0] == 'A' || detail[0] == 'a' || detail[0] == 'E' || detail[0] == 'e' || detail[0] == 'I' || detail[0] == 'i' || detail[0] == 'O' || detail[0] == 'o' || detail[0] == 'U' || detail[0] == 'u')
                    {
                        reply.Text = $"Great. You will be going to {Connections.ReformatString(location).Result} for an {detail}.";
                    }
                    else
                    {
                        reply.Text = $"Great. You will be going to {Connections.ReformatString(location).Result} for a {detail}.";
                    }
                }
                else
                {
                    reply.Text = $"What else would you like to know about Work Order {workOrderNumber.ToUpperInvariant()}?";
                }
                    
                HeroCard heroCard = new HeroCard();
                heroCard.Buttons = new List<CardAction>() {


                new CardAction() { Type = "imBack", Title = "View Safety Summary", Value = $"What is the Safety Summary for {workOrderNumber.ToUpper()}?" },
                new CardAction() { Type = "imBack", Title = "View PPE Required", Value = $"What PPE is required for Work Order {workOrderNumber.ToUpper()}?" },
                new CardAction() { Type = "imBack", Title = "View Reported Hazards", Value = $"What reported hazards are relevant to Work Order {workOrderNumber.ToUpper()}?" },
                new CardAction() { Type = "imBack", Title = "Return to Job List", Value = "Lets look at another Work Order" },
                new CardAction() { Type = "imBack", Title = "Return to Main Menu", Value = "Home" },};

                reply.Attachments = new List<Attachment>() { heroCard.ToAttachment() };
                context.UserData.SetValue<string>($"Work Order {workOrderNumber}", "Done");
                await context.PostAsync(reply);
                context.Done<IMessageActivity>(null);

                return;
            }
         /*
            await ResumeAfterProcessWorkOrder(context, , workOrderNumber, location);
        }
    }


    public async Task ResumeAfterProcessWorkOrder(IDialogContext context,IAwaitable<IMessageActivity> activity, string workOrderNumber, string location)
    {

        IMessageActivity message = await activity;
        dynamic value = message.Value;
        */

        }

        [LuisIntent("SafetyBriefing")]
        public async Task SafetyBriefing(IDialogContext context, IAwaitable<IMessageActivity> activity, LuisResult result)
        {
            EntityRecommendation recommendationLocation;
            EntityRecommendation recommendationWorkOrderNumber;
            await context.PostAsync("Let me get those details for you now...");

            bool hasaLocation = result.TryFindEntity(entityLocation, out recommendationLocation);
            bool hasaWorkOrderNumber = result.TryFindEntity(entityWorkOrderNumber, out recommendationWorkOrderNumber);
            string workOrderNumber = recommendationWorkOrderNumber.Entity;

            if (hasaLocation)
            {
                string location = await Connections.GetWorkOrderLocationAsync(recommendationLocation.Entity);
                List<string> locationBriefings = await Connections.GetBriefingByLocation(location);

                StringBuilder stringBuilderSafety = new StringBuilder();
                if (locationBriefings.Count != 0)
                {

                    stringBuilderSafety.Append("Please be aware of the following safety notifications for this location:\n\n");

                    foreach (string briefing in locationBriefings)
                    {
                        stringBuilderSafety.Append($"* {briefing}\n\n");
                    }

                    await context.PostAsync(stringBuilderSafety.ToString());
                }
            }
            else if (hasaWorkOrderNumber)
            {
                try
                {
                    string location = await Connections.GetWorkOrderLocationAsync(recommendationWorkOrderNumber.Entity);
                    string workOrderDetail = await Connections.GetWorkOrderDetailAsync(recommendationWorkOrderNumber.Entity);
                     
                    int incidentSerious = await Connections.GetSeriousNumber(workOrderDetail);
                    int incidentPotential = await Connections.GetPotentialNumber(workOrderDetail);
                    int incidentElectrical = await Connections.GetElectricalNumber(workOrderDetail);

                    List<string> locationBriefings = await Connections.GetBriefingByLocation(location);

                    StringBuilder stringBuilderSafety = new StringBuilder();
                    StringBuilder stringBuilderIncidents = new StringBuilder();
                    if (locationBriefings.Count != 0)
                    {
                        
                        stringBuilderSafety.Append("Please be aware of the following safety briefings for this location:\n\n");

                        foreach (string briefing in locationBriefings)
                        {
                            stringBuilderSafety.Append($"* {briefing}\n\n");
                        }

                        if (incidentSerious > 1)
                        {
                            stringBuilderIncidents.Append($"* Please be aware that there were {incidentSerious} reported Serious incidents for this work type last year\n\n\n\n");
                        }
                        else if (incidentSerious == 1)
                        {
                            stringBuilderIncidents.Append($"* Please be aware that there was {incidentSerious} reported Serious incident for this work type last year\n\n\n\n");
                        }
                        if (incidentPotential > 1)
                        {
                            stringBuilderIncidents.Append($"* Please be aware that there were {incidentPotential} reported Potentially Serious incidents for this work type last year.\n\n\n\n");
                        }
                        else if (incidentPotential == 1)
                        {
                            stringBuilderIncidents.Append($"* Please be aware that there was {incidentPotential} reported Potentially Serious incident for this work type last year.\n\n\n\n");
                        }
                        if (incidentElectrical > 1)
                        {
                            stringBuilderIncidents.Append($"* Please be aware that there were {incidentElectrical} reported Electrical incidents for this work type last year.\n\n\n\n");
                        }
                        else if (incidentElectrical == 1)
                        {
                            stringBuilderIncidents.Append($"* Please be aware that there was {incidentElectrical} reported Electrical incident for this work type last year.\n\n\n\n");
                        }
                        await context.PostAsync(stringBuilderSafety.ToString());
                        //await context.PostAsync(stringBuilderIncidents.ToString());
                        await Connections.UserCompleteSafetyBriefing(context.UserData.GetValueOrDefault("UserPrincipalName", string.Empty));

                        IMessageActivity reply = context.MakeMessage();
                        reply.Text = stringBuilderIncidents.ToString();
                        HeroCard heroCard = new HeroCard();
                        heroCard.Buttons = new List<CardAction>() {
                            new CardAction() { Type = "imBack", Title = "View Incidents", Value = $"Detailed Public Incident information for {workOrderNumber.ToUpper()}"} };
                                                        
                        reply.Attachments = new List<Attachment>() { heroCard.ToAttachment() };
                        await context.PostAsync(reply);
                    }

                }
                catch (Exception e)
                {
                    await context.PostAsync(e.Message);
                }
            }
            else
            {
                await context.PostAsync("No work order found.");
            }
            //await ProcessWorkOrderQuery(context, recommendationWorkOrderNumber.Entity);
            await Task.Delay(1500);
            await context.PostAsync("What would you like to do next?");
            context.Done<IMessageActivity>(null);


        }

        [LuisIntent("QuerySam")]
        public async Task WhoAreYou(IDialogContext context, IAwaitable<IMessageActivity> activity, LuisResult result)
        {
            EntityRecommendation recommendationQuerySam;
            bool hasaLocation = result.TryFindEntity(entityQuerySam, out recommendationQuerySam);

            string querySam = recommendationQuerySam.Entity;

            if (querySam == "who")
            {
                await context.PostAsync("I am Sam - your personal safety assistant");
            }
            else if (querySam == "what")
            {
                await context.PostAsync("I am an Artificial Intelligence chat bot created on the Microsoft Bot Framework. I have been trained" +
                    " to understand your intent, and help you in much the same way a person would - and often quicker.");
            }
            else if (querySam == "how")
            {
                await context.PostAsync("I use the LUIS cogntitive services to work out your intent. I am then able to help you - much quicker than a person would " +
                    "be able to. ");
            }
            context.Done<IMessageActivity>(null);

        }


        [LuisIntent("HowAreYou")]
        public async Task HowAreYou(IDialogContext context, IAwaitable<IMessageActivity> activity, LuisResult result)
        {
            DateTime aDateTime = new DateTime();
            Random r = new Random();
            int GetRandomInt(int min, int max)
            {
                return r.Next(min, max);
            }

            int rand = GetRandomInt(1, 5);

            if (rand == 1)
            {
                await context.PostAsync("I'm doing well, thank you. Now where did I put my coffee...");
            }
            else if (rand == 2)
            {
                await context.PostAsync("I think I have caught a bug... these data centres are so cold.");
            }
            else if (rand == 3)
            {
                await context.PostAsync("Never been better!");
            }
            else if (rand == 4)
            {
                await context.PostAsync("Fantastic. Its a great day.");
            }
            else if (rand == 5)
            {
                await context.PostAsync("Great!");
            }


            context.Done<IMessageActivity>(null);
        }
        [LuisIntent("EmailPublicIncident")]
        public async Task EmailPublicIncident(IDialogContext context, IAwaitable<IMessageActivity> activity, LuisResult result)
        {
            EntityRecommendation recommendationIncidentReferenceID;
            bool hasIncident = result.TryFindEntity(entityReferenceID, out recommendationIncidentReferenceID);
            await context.PostAsync("I will send those details through to you now.");
            await Task.Delay(1500);
            await context.PostAsync("What would you like to do next?");

            //Connections.SendPublicIncidentEmail(recommendationIncidentReferenceID.Entity);
            context.Done<IMessageActivity>(null);
        }
        [LuisIntent("CheckPublicData")]
        public async Task CheckPublicData(IDialogContext context, IAwaitable<IMessageActivity> activity, LuisResult result)
        {
            EntityRecommendation recommendationWorkOrderNumber;
            EntityRecommendation recommendationDetail;

            bool hasaWorkOrderNumber = result.TryFindEntity(entityWorkOrderNumber, out recommendationWorkOrderNumber);

            bool hasIncidentDetail = result.TryFindEntity(entityIncidentDetail, out recommendationDetail);

            if (hasaWorkOrderNumber)
            {
                
                string workOrderNumber = recommendationWorkOrderNumber.Entity;

                string detail = await Connections.GetWorkOrderDetailAsync(workOrderNumber);

                List<PublicIncident> publicIncidents = await Connections.PublicDataIncidents(detail);
                publicIncidents = (from incident in publicIncidents orderby incident.Occurence descending select incident).ToList();

                //Make Carousel Cards
                var reply = context.MakeMessage();
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment>();

                // Cards
                int numberOfIncidents = publicIncidents.Count;
                await context.PostAsync($"I have found {numberOfIncidents} incidents of this type.");
                foreach (PublicIncident incident in publicIncidents)
                {
                    List<CardAction> cardButtons = new List<CardAction>();
                    CardAction plButton = new CardAction()
                    {
                        Value = $"Email me the details of Incident {incident.ReferenceID}",
                        Type = "postBack",
                        Title = "Email me the full details"
                    };

                    cardButtons.Add(plButton);
                    if (incident.Detail.Length > 250 )
                    {
                        HeroCard plCard = new HeroCard()
                        {
                            Title = incident.Occurence.Date.ToString("dd MMM yyyy"),
                            Subtitle = incident.Detail.Substring(0, 250) + "...",
                            Buttons = cardButtons
                        };
                        Attachment plAttachment = plCard.ToAttachment();
                        reply.Attachments.Add(plAttachment);
                    }
                    else
                    {
                        HeroCard plCard = new HeroCard()
                        {
                            Title = incident.Occurence.Date.ToString("dd MMM yyyy"),
                            Subtitle = incident.Detail,
                            Buttons = cardButtons
                        };
                        Attachment plAttachment = plCard.ToAttachment();
                        reply.Attachments.Add(plAttachment);
                    }
                }

                reply.AttachmentLayout = "carousel";
                await context.PostAsync(reply);
            }
            else if (hasIncidentDetail)
            {
                string incidentDetail = recommendationDetail.Entity;
                List<PublicIncident> publicIncidents = await Connections.PublicDataIncidents(incidentDetail);

                //Make Carousel Cards
                var reply = context.MakeMessage();
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment>();

                int numberOfIncidents = publicIncidents.Count;
                await context.PostAsync($"I have found {numberOfIncidents} incidents of this type.");

                // Card #One
                foreach (PublicIncident incident in publicIncidents)
                {
                    List<CardAction> cardButtons = new List<CardAction>();
                    CardAction plButton = new CardAction()
                    {
                        Value = $"Email me the details of Incident {incident.ReferenceID}",
                        Type = "postBack",
                        Title = "Email me the full details"
                    };

                    cardButtons.Add(plButton);
                    if (incident.Detail.Length > 250)
                    {
                        HeroCard plCard = new HeroCard()
                        {
                            Title = incident.Occurence.Date.ToString("dd MMM yyyy"),
                            Subtitle = incident.Detail.Substring(0, 250) + "...",
                            Buttons = cardButtons
                        };
                        Attachment plAttachment = plCard.ToAttachment();
                        reply.Attachments.Add(plAttachment);
                    }
                    else
                    {
                        HeroCard plCard = new HeroCard()
                        {
                            Title = incident.Occurence.Date.ToString("dd MMM yyyy"),
                            Subtitle = incident.Detail,
                            Buttons = cardButtons
                        };
                        Attachment plAttachment = plCard.ToAttachment();
                        reply.Attachments.Add(plAttachment);
                    }
                }
                reply.AttachmentLayout = "carousel";
                await context.PostAsync(reply);
            }
            else
            {
                await context.PostAsync("I'm sorry, I don't understand that task.");
            }

            return;
        }

        
        [LuisIntent("CheckPermits")]
        public async Task CheckPermits(IDialogContext context, IAwaitable<IMessageActivity> activity, LuisResult result)
        {
            EntityRecommendation recommendationWorkOrderNumber;

            bool hasaWorkOrderNumber = result.TryFindEntity(entityWorkOrderNumber, out recommendationWorkOrderNumber);

            StringBuilder stringBuilderPermit = new StringBuilder();

            string location = await Connections.GetWorkOrderLocationAsync(recommendationWorkOrderNumber.Entity);

                try
                {
                    List<string> permitsMissing = await Connections.GetPermitByWorkOrder(recommendationWorkOrderNumber.Entity, context.UserData.GetValueOrDefault("UserPrincipalName", string.Empty));

                    if (permitsMissing.Count != 0)
                    {
                    stringBuilderPermit.Append($"The following permit is missing for Work Order {recommendationWorkOrderNumber.Entity.ToUpper()}:\n\n");

                    foreach (string permit in permitsMissing)
                        {
                            stringBuilderPermit.Append($"* {permit}\n\n");
                        }

                        stringBuilderPermit.Append($"\n\n Do not begin on this work order until you have completed the relevant learning material and courses.");

                        await context.PostAsync(stringBuilderPermit.ToString());
                    }
                    else if (location == null || location == string.Empty)
                    {
                        await context.PostAsync("Sorry, I cannot find those work order details.");
                    return;
                    }

                    else
                    {
                        await context.PostAsync("You have all the required permits for this job.");
                    }
                await Connections.UserCompletePermitCheck(context.UserData.GetValueOrDefault("UserPrincipalName", string.Empty));
                }
                catch (Exception e)
                {
                    await context.PostAsync(e.Message);
                }

            StringBuilder stringBuilderExpiry = new StringBuilder();
            try
            {
                List<string> permitsExpiring = await Connections.GetExpiringPermits(recommendationWorkOrderNumber.Entity, context.UserData.GetValueOrDefault("UserPrincipalName", string.Empty));

                if (permitsExpiring.Count != 0)
                {

                    stringBuilderExpiry.Append("Please be aware that your following permit(s) required for this job are expiring soon :\n\n");

                    foreach (string permit in permitsExpiring)
                    {
                        stringBuilderExpiry.Append($"* {permit}\n\n");
                    }

                    //await context.PostAsync(stringBuilderExpiry.ToString());

                    IMessageActivity reply = context.MakeMessage();
                    reply.Text = stringBuilderExpiry.ToString() + "\n\n Would you like to book a refresher course for these permits?";
                    //reply.Text = $"Would you like to book a refresher course for these permits?";


                    HeroCard heroCard = new HeroCard();
                    heroCard.Buttons = new List<CardAction>()
                {
                    new CardAction() { Type = "imBack", Title = "Yes", Value = $"I would like to book a refresher course for Permit {permitsExpiring[0]}" },
                    new CardAction() { Type = "imBack", Title = "No", Value = $"Back to Safety Information for Work Order {recommendationWorkOrderNumber.Entity.ToUpper()}"},};

                    reply.Attachments = new List<Attachment>() { heroCard.ToAttachment() };
                    await context.PostAsync(reply);

                    context.Done<IMessageActivity>(null);
                }
                else
                {
                    //await ProcessWorkOrderQuery(context, recommendationWorkOrderNumber.Entity);
                    context.Done<IMessageActivity>(null);
                }
            }
            catch (Exception e)
            {
                await context.PostAsync(e.Message);
            }
        }

        [LuisIntent("PPERequired")]
        public async Task PPERequired(IDialogContext context, IAwaitable<IMessageActivity> activity, LuisResult result)
        {
            EntityRecommendation recommendationWorkOrderNumber;

            bool hasWorkOrderNumber = result.TryFindEntity(entityWorkOrderNumber, out recommendationWorkOrderNumber);

            if (hasWorkOrderNumber)
            {
                List<string> ppeRequired = await Connections.GetPPEByWorkOrderNumber(recommendationWorkOrderNumber.Entity);
                ////////////////
                AdaptiveCard ppeCard = new AdaptiveCard();
                ppeCard.Body.Add(new TextBlock()
                {
                    Text = "PPE Order Menu",
                    Size = TextSize.Large,
                    Weight = TextWeight.Bolder
                });
                ppeCard.Body.Add(new TextBlock()
                {
                    Text = $"The following PPE is required for Work Order {recommendationWorkOrderNumber.Entity.ToUpperInvariant()}. Please complete the form to order any missing items.",
                    Wrap = true
                });

                ppeCard.Body.Add(new TextBlock()
                {
                    Text = "Item",
                    Weight = TextWeight.Bolder
                });

                List<AdaptiveCards.Choice> choices = new List<AdaptiveCards.Choice>();

                foreach (string ppe in ppeRequired)
                {
                    choices.Add(new AdaptiveCards.Choice() { Title = ppe, Value = ppe });
                }

                ppeCard.Body.Add(new ChoiceSet()
                {
                    Id = "Item",
                    Style = ChoiceInputStyle.Compact,
                    IsMultiSelect = true,
                    IsRequired = true,
                    Choices = choices
                });

                ppeCard.Body.Add(new TextBlock()
                {
                    Text = "Pickup Location",
                    Weight = TextWeight.Bolder
                });
                Random random = new Random();

                if (random.Next(1, 10) > 5)
                {
                    ppeCard.Body.Add(new TextBlock()
                    {
                        Text = "The closest pickup location to you is North Site - Supply Centre",
                        Wrap = true
                    });

                }
                else
                {
                    ppeCard.Body.Add(new TextBlock()
                    {
                        Text = "The closest pickup location to you is East Site - Supply Centre",
                        Wrap = true
                    });
                }


                ppeCard.Body.Add(new ChoiceSet()
                {
                    Id = "Locations",
                    Style = ChoiceInputStyle.Compact,
                    IsMultiSelect = false,
                    IsRequired = false,
                    Choices = new List<AdaptiveCards.Choice>()
                {
                    new AdaptiveCards.Choice() { Title = "North Site - Supply Centre", Value = "North Site, Supply Centre" },
                    new AdaptiveCards.Choice() { Title = "East Site - Vending Machine 2", Value = "East Site, Vending Machine 2" },
                    new AdaptiveCards.Choice() { Title = "East Site - Supply Centre", Value = "East Site, Supply Centre" },
                },
                    Value = "North Site - Supply Centre"
                });

                ppeCard.Actions.Add(new SubmitAction()
                {
                    Title = "I have all of these items",
                    DataJson = JObject.Parse("{ \"FormSubject\": \"PPE\", \"FormType\": \"PPENoneRequired\" }").ToString()
                });

                ppeCard.Actions.Add(new SubmitAction()
                {
                    Title = "Submit Order",
                    DataJson = JObject.Parse("{ \"FormSubject\": \"PPE\", \"FormType\": \"PPEOrderForm\" }").ToString()

                });

                Attachment resourceCardAttachment = new Attachment()
                {
                    ContentType = AdaptiveCard.ContentType,
                    Content = ppeCard
                };

                IMessageActivity resourceFormMessage = context.MakeMessage();
                resourceFormMessage.Attachments = new List<Attachment>();
                resourceFormMessage.Attachments.Add(resourceCardAttachment);

                await context.PostAsync(resourceFormMessage);
                context.Done<IMessageActivity>(null);
            }
            else
            {
                await context.PostAsync("What location or Work Order would you like details about?");
            }
        }

            
        //////////////

        /*
         *             StringBuilder stringBuilderPPE = new StringBuilder();
        if (ppeRequired.Count != 0)
        {
            stringBuilderPPE.Append($"The following PPE is required for Work Order {recommendationWorkOrderNumber.Entity.ToUpper()}:\n\n");

            foreach (string ppe in ppeRequired)
            {
                stringBuilderPPE.Append($"* {ppe}\n\n");
            }

            await context.PostAsync(stringBuilderPPE.ToString());
            await Connections.UserCompletePPERequired(context.UserData.GetValueOrDefault("UserPrincipalName", string.Empty));
        }
        else
        {
            await context.PostAsync("No PPE listed.");
        }
        //await ProcessWorkOrderQuery(context, recommendationWorkOrderNumber.Entity);
        await context.PostAsync("What would you like to do next?");
        context.Done<IMessageActivity>(null);
    }

    /*


        StringBuilder stringBuilderExpiry = new StringBuilder();
        List<string> permitsExpiring = await Connections.GetExpiringPermits(workOrderNumber, context.UserData.GetValueOrDefault("UserPrincipalName", string.Empty));
        if (permitsExpiring.Count != 0)
        {

            stringBuilderExpiry.Append("Please be aware that your following permit(s) required for this job are expiring soon :\n\n");

            foreach (string permit in permitsExpiring)
            {
                stringBuilderExpiry.Append($"* {permit}\n\n");
            }

            await context.PostAsync(stringBuilderExpiry.ToString());
        }

        StringBuilder stringBuilderHazard = new StringBuilder();
        List<string> hazardReports = await Connections.GetHazardsByLocation(location);
        if (hazardReports.Count != 0)
        {

            stringBuilderHazard.Append("Please be aware of the following unresolved hazards for the location:\n\n");

            foreach (string hazard in hazardReports)
            {
                stringBuilderHazard.Append($"* {hazard}\n\n");
            }

            await context.PostAsync(stringBuilderHazard.ToString());
        }

        Messages.Menu(context);
        context.Done<IMessageActivity>(null);
        */




        [LuisIntent("CreateReport")]
        public async Task CreateReport(IDialogContext context, IAwaitable<IMessageActivity> activity, LuisResult result)
        {
            IMessageActivity message = await activity;

            dynamic value = message.Value;

            EntityRecommendation recommendationHazard;
            EntityRecommendation recommendationLocation;

            bool hashazard = result.TryFindEntity(entityHazard, out recommendationHazard);
            bool haslocation = result.TryFindEntity(entityLocation, out recommendationLocation);

            HazardReportForm hazardReportForm = new HazardReportForm();

            if (haslocation)
            {

                string reportLocation = mLuisApplication.GetCanonicalForm(entityLocation, recommendationLocation.Entity);
                hazardReportForm.Location = GetLocationType(reportLocation);

            }
            else

            if (hashazard && haslocation)
            {
                hazardReportForm.HazardDetail = message.Text;
            }

            try
            {
                await context.Forward(new FormDialog<HazardReportForm>(hazardReportForm, HazardReportForm.BuildForm), ResumeAfterHazardReport, message);
            }
            catch (FormCanceledException<HazardReportForm> e)
            {
                await context.PostAsync(e.Message);
                string reply;
                if (e.InnerException == null)
                {
                    reply = $"You quit on {e.Last} -- maybe you can finish next time!";
                }
                else
                {
                    reply = "Sorry, I've had a short circuit. Please try again.";
                }
                await context.PostAsync(reply);
            }

            return;
     
        }


        private async Task ResumeAfterHazardReport(IDialogContext context, IAwaitable<HazardReportForm> awaitable)
        {
            HazardReportForm hazardReportForm = await awaitable;
            
            try
            {
                using (SqlConnection sqlConnection = Connections.GetSqlConnection())
                {
                    sqlConnection.Open();
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.Append("INSERT INTO [HazardReports] ([UserPrincipalName], [HazardDetail], [HazardLocation], [DateTimeofReport])");
                    stringBuilder.Append("VALUES (@UserPrincipalName, @HazardDetail, @HazardLocation, @DateTimeofReport);");

                    using (SqlCommand sqlCommand = new SqlCommand(stringBuilder.ToString(), sqlConnection))
                    {
                        sqlCommand.Parameters.AddWithValue("@UserPrincipalName", context.UserData.GetValueOrDefault("UserPrincipalName", string.Empty));
                        sqlCommand.Parameters.AddWithValue("@HazardDetail", new SqlString(hazardReportForm.HazardDetail));
                        sqlCommand.Parameters.AddWithValue("@HazardLocation", new SqlString(hazardReportForm.Location.ToString()));
                        sqlCommand.Parameters.AddWithValue("@DateTimeofReport", new SqlDateTime(DateTime.Parse(hazardReportForm.Occurrence.ToString())));

                       await sqlCommand.ExecuteNonQueryAsync();
                    }
                }
                await Connections.UserCompleteHazard(context.UserData.GetValueOrDefault("UserPrincipalName", string.Empty));

                //await BroadcastAlert(context, recommendationWorkOrderNumber.Entity);

                await context.PostAsync("Hazard Form Submitted.");
                await Connections.ReformatString(hazardReportForm.Location.ToString());
                IMessageActivity reply = context.MakeMessage();
                Random random = new Random();
                if( random.Next(1,2) == 1)
                {
                    reply.Text = "Based off the similarity of this report to previously submitted reports, I recommend broadcasting an alert. Would you like to do this?";
                    HeroCard heroCard = new HeroCard();
                    heroCard.Buttons = new List<CardAction>() {

                        new CardAction() { Type = "imBack", Title = "Yes", Value = $"Broadcast an alert at {Connections.ReformatString(hazardReportForm.Location.ToString()).Result}" },
                        new CardAction() { Type = "imBack", Title = "No", Value = $"Home" } };

                    reply.Attachments = new List<Attachment>() { heroCard.ToAttachment() };
                    await context.PostAsync(reply);
                }
               

            }
            catch (Exception e)
            {

                await context.PostAsync(e.Message);
                await context.PostAsync("There was an error submitting your Hazard Form. Please try again later.");
            }

            //await context.PostAsync(Messages.Menu(context));

            context.Done<IMessageActivity>(null);
        }

        [LuisIntent("ViewBroadcastDetails")] // This views broadcast details
        public async Task ViewBroadcastDetails(IDialogContext context, IAwaitable<IMessageActivity> awaitable, LuisResult result)
        {
            EntityRecommendation recommendationBroadcastNumber;
            bool hasBroadcastNumber = result.TryFindEntity(entityBuiltinNumber, out recommendationBroadcastNumber);
            context.Done < IMessageActivity>(null);
        }



        [LuisIntent("BroadcastAlert")] // This creates a broadcast alert
        public async Task BroadcastAlert(IDialogContext context, IAwaitable<IMessageActivity> awaitable, LuisResult result)
        {
            EntityRecommendation recommendationLocation;
            EntityRecommendation recommendationDetail;
            bool haslocation = result.TryFindEntity(entityLocation, out recommendationLocation);
            bool hasDetail = result.TryFindEntity(entityLocation, out recommendationDetail);

            if (haslocation == true)
            {
                await context.PostAsync($"An alert has been sent out for {Connections.ReformatString(recommendationLocation.Entity.ToString()).Result}.");

                await Connections.AddBroadcastNotification(Connections.ReformatString(recommendationLocation.Entity.ToString()).Result);
                try
                {
                    //await Connections.AddBroadcastSubscription();
                }
                catch
                {
                    //
                }
            }
            else
            {
                await context.PostAsync($"An alert has been sent out to nearby workers.");
            }

            //string aDetail = recommendationDetail.Entity;
            //await Connections.BroadcastAlert(aDetail);
            await context.PostAsync(Messages.Menu(context));
            context.Done<IMessageActivity>(null);
        }

        [LuisIntent("CalendarReminder")]
        public async Task CalendarReminder(IDialogContext context, IAwaitable<IMessageActivity> awaitable, LuisResult result)
        {
            await context.PostAsync("I have added a reminder to your calendar.");
            await Task.Delay(1500);
            await context.PostAsync("What would you like to do next?");
            context.Done<IMessageActivity>(null);
        }

        [LuisIntent("ClearNotification")]
        public async Task ClearNotification (IDialogContext context, IAwaitable<IMessageActivity> awaitable, LuisResult result)
        {
            EntityRecommendation recommendationNotificationType;
            EntityRecommendation recommendationBroadcastNumber;

            bool hasNotificationType = result.TryFindEntity(entityNotificationType, out recommendationNotificationType);
            bool hasBroadcastNumber = result.TryFindEntity(entityBuiltinNumber, out recommendationBroadcastNumber);


            string aNotification = recommendationNotificationType.Entity.ToString();
            if (recommendationNotificationType.Entity.ToString() == "broacastalert")
            {
                if (hasBroadcastNumber)
                {
                    int aBroadcastNumber = int.Parse(recommendationBroadcastNumber.Entity.ToString());
                    await Connections.ClearBroadcast(recommendationNotificationType.Entity, aBroadcastNumber);
                }
                else
                {
                    await Connections.ClearNotification(recommendationNotificationType.Entity);
                }
            }
            else if (recommendationNotificationType.Entity.ToString() == "safetysuggestion")
            {
                if (hasBroadcastNumber)
                {
                    int aBroadcastNumber = int.Parse(recommendationBroadcastNumber.Entity.ToString());
                    await Connections.ClearBroadcast(recommendationNotificationType.Entity, aBroadcastNumber);
                }
                else
                {
                    await Connections.ClearNotification(recommendationNotificationType.Entity);
                }
            }
            else
            {
                await Connections.ClearNotification(recommendationNotificationType.Entity);
            }
/*
            if (recommendationNotificationType.Entity.ToString() !="broadcastalert")
            {
                //await context.PostAsync(hasNotificationType.ToString());
                await Connections.ClearNotification(recommendationNotificationType.Entity);
            }
            else
            {
                if (hasBroadcastNumber)
                {
                    int aBroadcastNumber = int.Parse(recommendationBroadcastNumber.Entity.ToString());
                    await Connections.ClearBroadcast(recommendationNotificationType.Entity, aBroadcastNumber);
                }
                else
                {
                    await Connections.ClearNotification(recommendationNotificationType.Entity);
                }
            }
            */
            await FetchAndDisplayNotifications(context);

            context.Done<IMessageActivity>(null);
        }

        [LuisIntent("SetTimer")]
        public async Task SetTimer(IDialogContext context, IAwaitable<IMessageActivity> awaitable, LuisResult result)
        {
            EntityRecommendation recommendationTime;
            EntityRecommendation recommendationTimePassed;
            bool hasTime = result.TryFindEntity(entityClassTime, out recommendationTime);
            string jsonAsStr = recommendationTime.Resolution["values"].ToString();

            JObject jsonObj = JObject.Parse(jsonAsStr.Substring(1, jsonAsStr.Length - 2));

            DateTime aDateTime = new DateTime();
            aDateTime = DateTime.Parse(jsonObj.GetValue("value").ToString());
            TimeSpan theDifference = aDateTime - DateTime.Now;


            await context.PostAsync("I'll remind you at " + aDateTime.AddHours(8).TimeOfDay.ToString()+".");
            await Task.Delay(1500);
            await context.PostAsync("What would you like to do next?");
            context.Done<IMessageActivity>(null);
        }

        [LuisIntent("SafetySuggestion")]
        public async Task SafeySuggestion(IDialogContext context, IAwaitable<IMessageActivity> awaitable, LuisResult result)
        {
            IMessageActivity message = await awaitable;

            dynamic value = message.Value;

            SafetySuggestionForm suggestionForm = new SafetySuggestionForm();

                       try
            {
                await context.Forward(new FormDialog<SafetySuggestionForm>(suggestionForm, SafetySuggestionForm.BuildForm), ResumeAfterSafetySuggestion, message);
            }
            catch (FormCanceledException<SafetySuggestionForm> e)
            {
                await context.PostAsync(e.Message);
                string reply;
                if (e.InnerException == null)
                {
                    reply = $"You quit on {e.Last} -- maybe you can finish next time!";
                }
                else
                {
                    reply = "Sorry, I've had a short circuit. Please try again.";
                }
                await context.PostAsync(reply);
            }

            return;
        }

        private async Task ResumeAfterSafetySuggestion(IDialogContext context, IAwaitable<SafetySuggestionForm> awaitable)
        {
            SafetySuggestionForm suggestionForm = await awaitable;

            await Connections.AddSuggestionNotification(Connections.ReformatString(suggestionForm.Location.ToString()).Result,suggestionForm.HazardDetail.ToString());
            try
            {
                //await Connections.AddSuggestionSubscription();
            }
            catch
            {
                //
            }
            Random r = new Random();
            string assignedPersonnel = "";
            int GetRandomInt(int min, int max)
            {
                return r.Next(min, max);
            }

            int rand = GetRandomInt(1, 5);

            if (rand == 1)
            {
                assignedPersonnel = "Greg Phillips";
            }
            else if (rand == 2)
            {
                assignedPersonnel = "Chris O'Keeffe";
            }
            else if (rand == 3)
            {
                assignedPersonnel = "Samantha Wells";
            }
            else if (rand == 4)
            {
                assignedPersonnel = "Michael Turner";
            }
            else if (rand == 5)
            {
                assignedPersonnel = "Jane Wilson";
            }
            string dayOfWeek = DateTime.Now.AddDays(2).DayOfWeek.ToString();
            if (dayOfWeek == "Saturday" || dayOfWeek == "Sunday")
            {
                dayOfWeek = "Monday";
            }
            
            await context.PostAsync($"Thank you for the feedback. {assignedPersonnel} is responsible for this location, and has been assigned your Safety Suggestion. You will get a follow up by {dayOfWeek}, and can view outstanding suggestions" +
                    " in your notifications");
                await Task.Delay(2000);

            //await context.PostAsync(Messages.Menu(context));
            await context.PostAsync(Messages.Menu(context));
            context.Done<IMessageActivity>(null);
            
        }

        [LuisIntent("CheckNotifications")]
        public async Task CheckNotifications(IDialogContext context, IAwaitable<IMessageActivity> awaitable, LuisResult result)
        {
            /*
            HeroCard heroCard = new HeroCard();
            heroCard.Buttons = new List<CardAction>() {
                new CardAction() { Title = "Notification Settings", Type = "imBack", Value = "I would like to change my notification settings" },
                new CardAction() { Title = "Return to Main Menu", Type = "imBack", Value = "Home" } };
                

            IMessageActivity reply = context.MakeMessage();
            */

            await FetchAndDisplayNotifications(context);

            //Messages.Menu(context);
            context.Done<IMessageActivity>(null);
        }
        
        public async Task FetchAndDisplayNotifications(IDialogContext context)
        {
            List<Notification> notifications = await Connections.GetNotifications(context.UserData.GetValueOrDefault("UserPrincipalName", string.Empty));
            notifications = (from notification in notifications orderby notification.Timestamp descending select notification).ToList();

            //Make Carousel Cards
            var reply = context.MakeMessage();
            reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
            reply.Attachments = new List<Attachment>();

            // Card #One
            string expiringPermits = await Connections.GetAllExpiringPermits();

            Attachment plAttachment = new Attachment();
            foreach (Notification notification in notifications)
            {

                List<CardAction> cardButtons = new List<CardAction>();
                if (notification.ID == 1)
                {
                    CardAction plButton = new CardAction()
                    {
                        Value = "Set Calendar Reminder with my supervisor for tomorrow",
                        Type = "imBack",
                        Title = "Set Calendar Reminder"

                    };
                    cardButtons.Add(plButton);
                }
                /*
                else if (notification.Type.ToString() == "BroadcastAlert")
                {
                    CardAction plButton = new CardAction()
                    {
                        Value = "View details of Broadcast alert "+notification.Id ,
                        Type = "imBack",
                        Title = "View Details"

                    };
                    cardButtons.Add(plButton);
                }
                */
                else if (notification.Type.ToString() == "Workload")
                {
                    CardAction plButton = new CardAction()
                    {
                        Value = "Set timer 20 minutes from now to take a break",
                        Type = "imBack",
                        Title = "Set 20 Minute Timer"
                    };
                    cardButtons.Add(plButton);
                }

                CardAction plButton2 = new CardAction()
                {
                    Value = $"Clear {notification.Type.ToString()+" "+notification.ID.ToString()} Notification",
                    Type = "imBack",
                    Title = "Clear"

                };
                cardButtons.Add(plButton2);
                if (notification.Type.ToString() == "BroadcastAlert")
                {
                    HeroCard plCard = new HeroCard()
                    {
                        Title = Connections.ReformatString(notification.Type.ToString()).Result +" " +notification.ID,
                        Subtitle = notification.Content.ToString(),
                        Buttons = cardButtons

                    };
                    plAttachment = plCard.ToAttachment();
                    reply.Attachments.Add(plAttachment);
                }
                else if (notification.Type.ToString() == "SafetySuggestion")
                {
                    HeroCard plCard = new HeroCard()
                    {
                        Title = Connections.ReformatString(notification.Type.ToString()).Result + " " + notification.ID,
                        Subtitle = "Status: Pending \n\n " + notification.Content.ToString(),
                        
                        Buttons = cardButtons

                    };
                    plAttachment = plCard.ToAttachment();
                    reply.Attachments.Add(plAttachment);
                }
                else
                {
                    HeroCard plCard = new HeroCard()
                    {
                        Title = Connections.ReformatString(notification.Type.ToString()).Result,
                        Subtitle = notification.Content.ToString(),
                        Buttons = cardButtons

                    };
                    plAttachment = plCard.ToAttachment();
                    reply.Attachments.Add(plAttachment);
                }                    
            }

            List<CardAction> cardButton = new List<CardAction>();
            CardAction plButton_permit = new CardAction()
            {
                Value = $"Book Refresher Course for Compentency {expiringPermits}",
                Type = "imBack",
                Title = $"Book Refresher Course"

            };
            cardButton.Add(plButton_permit);

            HeroCard plCard_permit = new HeroCard()
            {
                Title = "Competency " + expiringPermits,
                Subtitle = $"Your compentency {expiringPermits} will be expiring within the next 3 months. Would you like me to book a refresher course for you?",
                Buttons = cardButton

            };
            plAttachment = plCard_permit.ToAttachment();
            reply.Attachments.Add(plAttachment);

            reply.AttachmentLayout = "carousel";
            await context.PostAsync(reply);
            await Task.Delay(1500);
            await context.PostAsync("What would you like to do next?");

            await Connections.UserCompleteNotifications(context.UserData.GetValueOrDefault("UserPrincipalName", string.Empty));
        }

        [LuisIntent("ProvideSafetyWarnings")]
        public async Task ProvideSafetyWarnings(IDialogContext context, IAwaitable<IMessageActivity> activity, LuisResult result)
        {
            IMessageActivity message = await activity;

            List<NotificationType> enabledNotifications = await Connections.GetNotificationSubscriptions(context.UserData.GetValueOrDefault("UserPrincipalName", ""));

            if (enabledNotifications.Count != 0)
            {
                StringBuilder stringBuilder = new StringBuilder();

                foreach (NotificationType notification in enabledNotifications)
                {
                    stringBuilder.Append($"* {notification.ToString()}\n\n");
                }
                
                await context.PostAsync("You are currently subscribed to the following notifications:\n\n" + stringBuilder.ToString());
            }
            else
            {
                await context.PostAsync("You are currently not subscribed to any notifications.");
            }

            await context.Forward(new FormDialog<NotificationForm>(new NotificationForm(), NotificationForm.BuildForm), ResumeAfterNotificationFormDialog, message, CancellationToken.None);
            return;

        }

        public async Task ResumeAfterNotificationFormDialog(IDialogContext context, IAwaitable<NotificationForm> awaitable)
        {
            NotificationForm notificationForm = await awaitable;

            if (notificationForm.NotificationOperation == NotificationOperationType.Subscribe)
            {
                await Connections.SubscribeNotification(context.UserData.GetValueOrDefault("UserPrincipalName", string.Empty), notificationForm.Notification);
            }
            else if (notificationForm.NotificationOperation == NotificationOperationType.Unsubscribe)
            {
                await Connections.UnsubscribeNotification(context.UserData.GetValueOrDefault("UserPrincipalName", string.Empty), notificationForm.Notification);
            }

            await context.PostAsync($"You are now " + (notificationForm.NotificationOperation == NotificationOperationType.Subscribe ? "subscribed" : "unsubscribed")
                + $" to **{notificationForm.Notification.ToString()}** notifications, is there anything else I can help you with?");

            await context.PostAsync(Messages.Menu(context));
            
            context.Done<IMessageActivity>(null);
        }
        
        protected override IntentRecommendation BestIntentFrom(LuisResult result)
        {
            double maxScore = result.TopScoringIntent.Score ?? 0.0;
            for (int i = 0; i < result.Intents.Count; i++)
            {
                if ((result.Intents[i].Score ?? 0.0) > maxScore)
                {
                    maxScore = (result.Intents[i].Score ?? 0.0);
                }
            }

            if (maxScore < 0.4)
            {
                return new IntentRecommendation("", 1.0);
            }
            else
            {
                return result.TopScoringIntent ?? result.Intents?.MaxBy(i => i.Score ?? 0d);
            }
        }
    }
}
