﻿using Microsoft.Bot.Builder.Dialogs.Internals;
using Microsoft.Bot.Builder.Internals.Fibers;
using Microsoft.Bot.Builder.Scorables.Internals;
using Microsoft.Bot.Connector;
using System.Threading;
using System.Threading.Tasks;

namespace Mining_Safety_Bot
{
    public class FormScorable : ScorableBase<IActivity, IMessageActivity, double>
    {
        private readonly IDialogTask task;

        // Constructor.
        public FormScorable(IDialogTask task)
        {
            SetField.NotNull(out this.task, nameof(task), task);
        }

        // Check the received IAcvitivity and determine if we want to intercept it.
        // The object returned is passed to the HasScore, GetScore, and PostAync methods.
        protected override async Task<IMessageActivity> PrepareAsync(IActivity activity, CancellationToken token)
        {
            IMessageActivity message = (IMessageActivity)activity;

            if (message == null)
            {
                return null;
            }
            else if (message.Text == null && message.Value != null)
            {
                return message;
            }
            else
            {
                return null;
            }
        }

        // Returns true if we want to place a claim on responding to the dialog.
        // Returns false if we don't want to process it.
        protected override bool HasScore(IActivity item, IMessageActivity message)
        {
            return message != null;
        }

        // Returns a score from 0 to 1 on how strongly we want to process this message.
        // This acts as a priority determinant if multiple scorables return a true for HasScore.
        protected override double GetScore(IActivity item, IMessageActivity message)
        {
            return 1.0;
        }

        // If the scorable has the highest score, PostAsync is called.
        protected override async Task PostAsync(IActivity item, IMessageActivity message, CancellationToken token)
        {
            await this.task.InterruptAsync(new FormDialog(), message, token);
        }

        // This is called when everything is finalized.
        protected override Task DoneAsync(IActivity item, IMessageActivity state, CancellationToken token)
        {
            return Task.CompletedTask;
        }
    }
}