﻿using System;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using System.Threading;

namespace Mining_Safety_Bot
{
    [Serializable]
    public class FormDialog : IDialog<IMessageActivity>
    {

        public async Task StartAsync(IDialogContext context)
        {
            // Just wait for a message.
            context.Wait(MessageReceivedAsync);
        }

        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            IMessageActivity message = await result;

            dynamic value = message.Value;
            switch (value.FormSubject.ToString())
            {
                case "PPE":
                    // The form is abouta ppe order, send it to be processed by a separate dialog.
                    await context.Forward(new PpeDialog(), FormSubmitAfter, message, CancellationToken.None);
                    break;
                case "HazardQuery":
                    // The form is abouta ppe order, send it to be processed by a separate dialog.
                    await context.Forward(new ViewHazardDialog(), FormSubmitAfter, message, CancellationToken.None);
                    break;
                case "HazardUpdate":
                    await context.PostAsync("Your additions have been submitted. Thank you.");
                    await Task.Delay(1500);
                    await context.PostAsync("What would you like to do next?");
                    break;
                default:
                    // Do nothing.
                    break;
            }

            context.Done<IMessageActivity>(null);
        }

        private async Task FormSubmitAfter(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            IMessageActivity message = await result;

            context.Done<IMessageActivity>(null);
        }
    }
}