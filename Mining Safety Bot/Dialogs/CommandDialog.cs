﻿using System;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using System.Text;

namespace Mining_Safety_Bot
{
    [Serializable]
    public class CommandDialog : IDialog<IMessageActivity>
    {

        public async Task StartAsync(IDialogContext context)
        {
            // Just wait for a message.
            context.Wait(MessageReceivedAsync);
        }

        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> activity)
        {
            IMessageActivity message = await activity;

            // Check if it is a pre-defined command.

            if (message.Text.ToLowerInvariant() == "!info")
            {
                // Print some information
                await context.PostAsync($"Bot Id: {message.Recipient.Id} ({message.Recipient.Name})\n\nChannel: {message.ChannelId}\n\nConversation: {message.Conversation.Id} ({message.Conversation.Name})\n\nSender: {message.From.Id} ({message.From.Name})");
            }
            else if (message.Text.ToLowerInvariant() == "!testtimer")
            {
                await context.PostAsync("checkpoint2");
            }
            else if (message.Text.ToLowerInvariant() == "!email")
            {
                try
                {
                    await Connections.SendInvite(context.UserData.GetValueOrDefault("UserPrincipalName", string.Empty), DateTime.Now, "34567");
                    await context.PostAsync("checkpoint1");
                }
                catch (Exception e)
                {
                    await context.PostAsync(e.Message);
                }
            }
            else if (message.Text.ToLowerInvariant() == "!cleandatabase")
            {
                await Connections.CleanDatabase();
                await context.PostAsync("Database cleaned");
            }
            else if (message.Text.ToLowerInvariant() == "!cleannotifications")
            {
                await Connections.CleanNotifications();
                await context.PostAsync("Notifications Cleaned");
            }
            else if (message.Text.ToLowerInvariant() == "!echo")
            {
                await context.PostAsync(message.Text);
            }
            else if (message.Text.ToLowerInvariant() == "!participants")
            {
                // Print information about the participants
                StringBuilder stringBuilder = new StringBuilder();
                ChannelAccount[] participants = new ConnectorClient(new Uri(message.ServiceUrl)).Conversations.GetConversationMembers(message.Conversation.Id);

                stringBuilder.Append("Participants:\n\n");

                for (int i = 0; i < participants.Length; i++)
                {
                    ChannelAccount participant = participants[i];
                    stringBuilder.Append($"{participant.Id} ({participant.Name})");
                    if (i != participants.Length - 1)
                    {
                        stringBuilder.Append("\n\n");
                    }
                }

                await context.PostAsync(stringBuilder.ToString());
            }
            else if (message.Text.ToLowerInvariant().StartsWith("!set"))
            {
                string[] stringTokens = message.Text.Split(' ');
                if (stringTokens.Length == 4)
                {
                    switch (stringTokens[1])
                    {
                        case "bool":
                            if (stringTokens[3] == "true")
                            {
                                context.UserData.SetValue<bool>(stringTokens[2], true);
                            }
                            else
                            {
                                context.UserData.SetValue<bool>(stringTokens[2], false);
                            }
                            await context.PostAsync("Set.");
                            break;
                        case "string":
                            context.UserData.SetValue<string>(stringTokens[2], stringTokens[3]);
                            await context.PostAsync("Set.");
                            break;
                    }
                }
            }
            else if (message.Text.ToLowerInvariant().StartsWith("!get"))
            {
                string[] stringTokens = message.Text.Split(' ');
                if (stringTokens.Length == 3)
                {
                    switch (stringTokens[1])
                    {
                        case "bool":
                            await context.PostAsync($"{context.UserData.GetValue<bool>(stringTokens[2]).ToString() ?? ""}");
                            break;
                        case "string":
                            await context.PostAsync($"{context.UserData.GetValue<string>(stringTokens[2]).ToString() ?? ""}");
                            break;
                    }
                }
            }
            else
            {
                // Do nothing.
            }

            context.Done<IMessageActivity>(null);
            

        }

            
            /*
            if (message.Text.ToLowerInvariant() == "!info")
            {
                // Print some information
                await context.PostAsync($"Bot Id: {message.Recipient.Id} ({message.Recipient.Name})\n\nChannel: {message.ChannelId}\n\nConversation: {message.Conversation.Id} ({message.Conversation.Name})\n\nSender: {message.From.Id} ({message.From.Name})");
            }
            else if(message.Text.ToLowerInvariant() == "!testtimer")
            {
                await context.PostAsync("checkpoint2");
            }
            else if(message.Text.ToLowerInvariant() == "!email")
            {
                try
                {
                    await Connections.SendInvite("chris.okeeffe@au.ey.com", DateTime.Now, "34567");
                    await context.PostAsync("checkpoint1");
                }
                catch (Exception e)
                {
                    await context.PostAsync(e.Message);
                }
            }
            else if (message.Text.ToLowerInvariant() == "!participants")
            {
                // Print information about the participants
                StringBuilder stringBuilder = new StringBuilder();
                ChannelAccount[] participants = new ConnectorClient(new Uri(message.ServiceUrl)).Conversations.GetConversationMembers(message.Conversation.Id);

                stringBuilder.Append("Participants:\n\n");

                for (int i = 0; i < participants.Length; i++)
                {
                    ChannelAccount participant = participants[i];
                    stringBuilder.Append($"{participant.Id} ({participant.Name})");
                    if (i != participants.Length - 1)
                    {
                        stringBuilder.Append("\n\n");
                    }
                }

                await context.PostAsync(stringBuilder.ToString());
            }
            else if (message.Text.ToLowerInvariant().StartsWith("!set"))
            {
                string[] stringTokens = message.Text.Split(' ');
                if (stringTokens.Length == 4)
                {
                    switch (stringTokens[1])
                    {
                        case "bool":
                            if (stringTokens[3] == "true")
                            {
                                context.UserData.SetValue<bool>(stringTokens[2], true);
                            }
                            else
                            {
                                context.UserData.SetValue<bool>(stringTokens[2], false);
                            }
                            await context.PostAsync("Set.");
                            break;
                        case "string":
                            context.UserData.SetValue<string>(stringTokens[2], stringTokens[3]);
                            await context.PostAsync("Set.");
                            break;
                    }
                }
            }
            else if (message.Text.ToLowerInvariant().StartsWith("!get"))
            {
                string[] stringTokens = message.Text.Split(' ');
                if (stringTokens.Length == 3)
                {
                    switch (stringTokens[1])
                    {
                        case "bool":
                            await context.PostAsync($"{context.UserData.GetValue<bool>(stringTokens[2]).ToString() ?? ""}");
                            break;
                        case "string":
                            await context.PostAsync($"{context.UserData.GetValue<string>(stringTokens[2]).ToString() ?? ""}");
                            break;
                    }
                }
            }
            else
            {
                // Do nothing.
            }

            context.Done<IMessageActivity>(null);
        }
        */
        
    }
}