﻿using System;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;

namespace Mining_Safety_Bot
{
    [Serializable]
    public class NameDialog : IDialog<IMessageActivity>
    {

        public async Task StartAsync(IDialogContext context)
        {
            // Just wait for a message.
            context.Wait(MessageReceivedAsync);
        }

        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            IMessageActivity message = await result;

            try
            {
                context.UserData.SetValue<string>("PreferredName", message.Text);

                IMessageActivity reply = context.MakeMessage();
                reply.Text = $"Thanks for that, {context.UserData.GetValue<string>("PreferredName")}.";
                await context.PostAsync(reply);
            }
            catch (Exception e)
            {
                await context.PostAsync($"{e.Message}");
            }

            context.Done<IMessageActivity>(null);
        }
    }
}