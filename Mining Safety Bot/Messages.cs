﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Mining_Safety_Bot
{
    public static class Messages
    {
        public static IMessageActivity Greeting(Activity aActivity, string aGivenName = null)
        {
            IMessageActivity message = aActivity.CreateReply();

            if (aGivenName == null)
            {
                message.Text = $"Hello, I am the  Mining Safety bot."+
                    "\n\n How can I help you today?";
            }
            else
            {
                message.Text = $"Hello, I am the Mining Safety Bot." +
                    "\n\n How can I help you today?";
            }

            HeroCard heroCard = new HeroCard();
            heroCard.Buttons = new List<CardAction>() {
                new CardAction() { Type = "imBack", Title = "Create a Hazard Report", Value = "Create a Hazard Report" },
                new CardAction() { Type = "imBack", Title = "Safety Briefing", Value = "I would like to complete a Safety Briefing" },
                new CardAction() { Type = "imBack", Title = "Safety Suggestion", Value = "I want to create a new safety suggestion" },
                new CardAction() { Type = "imBack", Title = "My Notifications", Value = "Are there any notifications for me?" }
            };

            message.Attachments = new List<Attachment>() { heroCard.ToAttachment() };

            return message;
        }

        public static IMessageActivity Menu(IDialogContext context)
        {
            try
            {
                IMessageActivity message = context.MakeMessage();

                message.Text = "What would you like to do next?";
                HeroCard heroCard = new HeroCard();
                heroCard.Buttons = new List<CardAction>() {
                    new CardAction() { Type = "imBack", Title = "Create a Hazard Report", Value = "Create a Hazard Report" },
                    new CardAction() { Type = "imBack", Title = "Safety Briefing", Value = "I would like to complete a Safety Briefing" },
                    new CardAction() { Type = "imBack", Title = "Safety Suggestion", Value = "I want to create a new safety suggestion" },
                    new CardAction() { Type = "imBack", Title = "My Notifications", Value = "Are there any notifications for me?" }
                };

                message.Attachments = new List<Attachment>() { heroCard.ToAttachment() };
                context.UserData.SetValue<string>("WorkOrder", string.Empty);
                return message;

            }
            catch (Exception e)
            {
                context.PostAsync(e.Message);
                return null;

            }
         
        }
        
    }
}