﻿using Microsoft.Graph;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Net;
using Microsoft.Exchange.WebServices.Data;
using System.Web.Services;
using System.Text;

namespace Mining_Safety_Bot
{
    public static class Connections
    {
        public static async Task<List<PublicIncident>> PublicDataIncidents(string workOrderDetail)
        {
            List<PublicIncident> incidentList = new List<PublicIncident>();

            using (SqlConnection sqlConnection = GetSqlConnection())
            {
                await sqlConnection.OpenAsync();

                using (SqlCommand sqlCommand = new SqlCommand("SELECT [Description], [Incident date], [Reference ID] FROM [dbo].[combined] WHERE [Description] LIKE @workOrderDetail ", sqlConnection))
                {
                    sqlCommand.Parameters.AddWithValue("@workOrderDetail", new SqlString("%" + workOrderDetail + "%"));

                    sqlCommand.ExecuteNonQuery();

                    SqlDataReader sqlDataReader = await sqlCommand.ExecuteReaderAsync();

                    while (await sqlDataReader.ReadAsync())
                    {
                        PublicIncident publicIncident = new PublicIncident()
                        {
                            Occurence = sqlDataReader.GetSqlDateTime(1).Value,
                            Detail = sqlDataReader.GetSqlString(0).Value,
                            ReferenceID = sqlDataReader.GetSqlString(2).Value,

                        };

                        incidentList.Add(publicIncident);
                    }
                }
            }
            
            return incidentList;
        }
        
        public static async System.Threading.Tasks.Task CleanDatabase()
        {
            using (SqlConnection sqlConnection = GetSqlConnection())
            {
                await sqlConnection.OpenAsync();

                using (SqlCommand sqlCommand = new SqlCommand(" DELETE [HazardReports] WHERE [CleanData] IS NULL", sqlConnection))
                {
                    sqlCommand.ExecuteNonQuery();
                }
            }
        }

        public static async System.Threading.Tasks.Task CleanNotifications()
        {
            using (SqlConnection sqlConnection = GetSqlConnection())
            {
                await sqlConnection.OpenAsync();

                using (SqlCommand sqlCommand = new SqlCommand(" DELETE [Notifications] WHERE [NotificationType] ='BroadcastAlert' OR [NotificationType] ='SafetySuggestion'", sqlConnection))
                {
                    sqlCommand.ExecuteNonQuery();
                }
            }

            using (SqlConnection sqlConnection = GetSqlConnection())
            {
                await sqlConnection.OpenAsync();

                using (SqlCommand sqlCommand = new SqlCommand(" Update [Notifications] set [IsRead] = 0", sqlConnection))
                {
                    sqlCommand.ExecuteNonQuery();
                }
            }
        }

        public static async System.Threading.Tasks.Task AddBroadcastNotification(string content)
        {
            using (SqlConnection sqlConnection = GetSqlConnection())
            {
                await sqlConnection.OpenAsync();

                using (SqlCommand sqlCommand = new SqlCommand("INSERT INTO [Notifications] ([DateTime], [UserPrincipalName] ,[NotificationType], [IsRead], [Content] ) " +
                    "VALUES(@Datetime,'chris.okeeffe@au.ey.com', 'BroadcastAlert', '0',@Content) ", sqlConnection))
                {
                    sqlCommand.Parameters.AddWithValue("@Datetime", new SqlDateTime(DateTime.Now));
                    sqlCommand.Parameters.AddWithValue("@Content", new SqlString($"An alert has been broadcasted for {content}. You have recieved this no" +
                        $"tification as you are in the vacinity. Please view the reported hazards."));

                    sqlCommand.ExecuteNonQuery();                    
                }
            }

        }

        public static async System.Threading.Tasks.Task AddBroadcastSubscription()
        {
            using (SqlConnection sqlConnection = GetSqlConnection())
            {
                await sqlConnection.OpenAsync();

                using (SqlCommand sqlCommand = new SqlCommand("INSERT INTO [NotificationSubscriptions] ([UserPrincipalName],[NotificationType]) " +
                    "VALUES('chris.okeeffe@au.ey.com', 'BroadcastAlert') ", sqlConnection))
                {
                    sqlCommand.ExecuteNonQuery();
                }
            }

        }

        public static async System.Threading.Tasks.Task AddSuggestionNotification(string location, string content)
        {
            using (SqlConnection sqlConnection = GetSqlConnection())
            {
                await sqlConnection.OpenAsync();

                using (SqlCommand sqlCommand = new SqlCommand("INSERT INTO [Notifications] ([DateTime], [UserPrincipalName] ,[NotificationType], [IsRead], [Content] ) " +
                    "VALUES(@Datetime,'chris.okeeffe@au.ey.com', 'SafetySuggestion', '0',@Content) ", sqlConnection))
                {
                    sqlCommand.Parameters.AddWithValue("@Datetime", new SqlDateTime(DateTime.Now));
                    sqlCommand.Parameters.AddWithValue("@Content", new SqlString($"{location}: {content}"));

                    sqlCommand.ExecuteNonQuery();
                }
            }

        }

        public static async System.Threading.Tasks.Task AddSuggestionSubscription()
        {
            using (SqlConnection sqlConnection = GetSqlConnection())
            {
                await sqlConnection.OpenAsync();

                using (SqlCommand sqlCommand = new SqlCommand("INSERT INTO [NotificationSubscriptions] ([UserPrincipalName],[NotificationType]) " +
                    "VALUES('chris.okeeffe@au.ey.com', 'SafetySuggestion') ", sqlConnection))
                {
                    sqlCommand.ExecuteNonQuery();
                }
            }

        }



        public static async Task<List<ExistingHazards>> CheckExistingHazards(string aLocation)
        {
            List<ExistingHazards> hazardList = new List<ExistingHazards>();

            using (SqlConnection sqlConnection = GetSqlConnection())
            {
                await sqlConnection.OpenAsync();

                using (SqlCommand sqlCommand = new SqlCommand("SELECT [HazardType], [HazardDetail], [HazardLocation], [DateTimeofReport] FROM [HazardReports]" +
                    "WHERE [HazardLocation] = @aLocation ORDER BY[DateTimeofReport] DESC", sqlConnection))
                {
                    sqlCommand.Parameters.AddWithValue("@aLocation", new SqlString(aLocation));

                    sqlCommand.ExecuteNonQuery();

                    SqlDataReader sqlDataReader = await sqlCommand.ExecuteReaderAsync();

                    while (await sqlDataReader.ReadAsync())
                    {
                        ExistingHazards existingHazards = new ExistingHazards()
                        {
                            Timestamp = sqlDataReader.GetSqlDateTime(3).Value,
                            Detail = sqlDataReader.GetSqlString(1).Value,

                        };

                        hazardList.Add(existingHazards);
                    }
                }
            }

            return hazardList;
        }


        public static async Task<string> SendHazardDetails(string principalUserName, string text)
        {
            string fromEmail = "mining.safety.sam@gmail.com";
            string fromPW = "1L0veEYC3";
            string toEmail = principalUserName;

            using (SmtpClient smtpClient = new SmtpClient("smtp.gmail.com", 587))
            {
                smtpClient.EnableSsl = true;
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = new NetworkCredential(fromEmail, fromPW);


                MailMessage msg = new MailMessage();

                msg.From = new MailAddress(fromEmail, "Mining Safety Bot");
                msg.To.Add(new MailAddress(toEmail));
                msg.Subject = "Hazard Report Details";
                msg.Body = "" +
                    "Enclosed are the details of the hazard report:\n\n" +
                    $"{text}" +
                    "";
                smtpClient.Send(msg);
            }
            return null;
        }


        public static async Task<string> SendInvite(string principalUserName, DateTime aClassTime, string aPermitNumber)
        {
            string fromEmail = "mining.safety.sam@gmail.com";
            string fromPW = "1L0veEYC3";
            string toEmail = principalUserName;

            using (SmtpClient smtpClient = new SmtpClient("smtp.gmail.com", 587))
            {
                smtpClient.EnableSsl = true;
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = new NetworkCredential(fromEmail, fromPW);


                MailMessage msg = new MailMessage();

                msg.From = new MailAddress(fromEmail, "Mining Safety Bot");
                msg.To.Add(new MailAddress(toEmail));
                msg.Subject = "Refresher Course for Permit " + aPermitNumber;
                msg.Body = "Hello, \n\n" +
                    "Attached to this email is your calendar invite renew Permit Number " + aPermitNumber + ".\n\n" +
                    "Please accept to add these details to your calendar.\n\n\n\n" +
                    "Please bring a notepad and pen. Resfreshements and sandwiches will be provided.\n\n\n\n";


                StringBuilder str = new StringBuilder();
                str.AppendLine("BEGIN:VCALENDAR");

                //PRODID: identifier for the product that created the Calendar object
                str.AppendLine("PRODID:-//ABC Company//Outlook MIMEDIR//EN");
                str.AppendLine("VERSION:2.0");
                str.AppendLine("METHOD:REQUEST");

                str.AppendLine("BEGIN:VEVENT");

                str.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", aClassTime));//TimeZoneInfo.ConvertTimeToUtc("BeginTime").ToString("yyyyMMddTHHmmssZ")));
                str.AppendLine(string.Format("DTSTAMP:{0:yyyyMMddTHHmmssZ}", aClassTime));
                str.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", aClassTime.AddHours(2)));//TimeZoneInfo.ConvertTimeToUtc("EndTime").ToString("yyyyMMddTHHmmssZ")));
                str.AppendLine(string.Format("LOCATION: {0}", "Room 1.23"));

                // UID should be unique.
                str.AppendLine(string.Format("UID:{0}", Guid.NewGuid()));
                str.AppendLine(string.Format("DESCRIPTION:{0}", msg.Body));
                str.AppendLine(string.Format("X-ALT-DESC;FMTTYPE=text/html:{0}", msg.Body));
                str.AppendLine(string.Format("SUMMARY:{0}", msg.Subject));

                str.AppendLine("STATUS:CONFIRMED");
                str.AppendLine("BEGIN:VALARM");
                str.AppendLine("TRIGGER:-PT15M");
                str.AppendLine("ACTION:Accept");
                str.AppendLine("DESCRIPTION:Reminder");
                str.AppendLine("X-MICROSOFT-CDO-BUSYSTATUS:BUSY");
                str.AppendLine("END:VALARM");
                str.AppendLine("END:VEVENT");

                str.AppendLine(string.Format("ORGANIZER:MAILTO:{0}", msg.From.Address));
                str.AppendLine(string.Format("ATTENDEE;CN=\"{0}\";RSVP=TRUE:mailto:{1}", msg.To[0].DisplayName, msg.To[0].Address));

                str.AppendLine("END:VCALENDAR");

                System.Net.Mime.ContentType ct = new System.Net.Mime.ContentType("text/calendar");
                ct.Parameters.Add("method", "REQUEST");
                ct.Parameters.Add("name", "meeting.ics");
                AlternateView avCal = AlternateView.CreateAlternateViewFromString(str.ToString(), ct);
                msg.AlternateViews.Add(avCal);
                //Response.Write(str);
                // sc.ServicePoint.MaxIdleTime = 2;

                //smtpClient.Send(msg.From.ToString(), msg.To.ToString(), msg.Subject, msg.Body);
                smtpClient.Send(msg);
            };
            


            /*
            string fromEmail = "mining.safety.sam@gmail.com";
            string fromPW = "1L0veEYC3";
            string toEmail = "chris.okeeffe@au.ey.com";

            
            MailMessage message = new System.Net.Mail.MailMessage();

            message.From = new MailAddress(fromEmail);
            message.To.Add(toEmail);
            message.Subject = "Hello";
            message.Body = "Hi " + principalUserName +". Here is a calendar invite for " + aClassTime + " for Permit Number " + aPermitNumber +".";
            message.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

            
            ExchangeService service = new ExchangeService();

            #region Authentication

            // Set specific credentials.
            service.UseDefaultCredentials = false;
            service.Credentials = new NetworkCredential(fromEmail, fromPW);
            #endregion



            // Create the appointment.
            Appointment appointment = new Appointment(service);

            // Set properties on the appointment. Add two required attendees and one optional attendee.
            appointment.Subject = "Resfresher Course for Permit Number " + aPermitNumber;
            appointment.Body = "The purpose of this meeting is to discuss status.";
            appointment.Start = aClassTime;
            appointment.End = appointment.Start.AddHours(2);
            appointment.Location = "Training Room 2";
            appointment.RequiredAttendees.Add(toEmail);


            // Send the meeting request to all attendees and save a copy in the Sent Items folder.
            appointment.Save(SendInvitationsMode.SendToAllAndSaveCopy);

            

            using (SmtpClient smtpClient = new SmtpClient("smtp.gmail.com", 587))
            {
                smtpClient.EnableSsl = true;
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = new NetworkCredential(fromEmail, fromPW);

                smtpClient.Send(message.From.ToString(), message.To.ToString(),
                                message.Subject, message.Body);
            }
            
            */
            return null;
        }


        public static GraphServiceClient GetGraphServiceClient(string accessToken)
        {
            return new GraphServiceClient(
                new DelegateAuthenticationProvider(
                (requestMessage) =>
                {
                    requestMessage.Headers.Authorization = new AuthenticationHeaderValue("bearer", accessToken);

                    return System.Threading.Tasks.Task.FromResult(0);
                }));
        }

        public static SqlConnection GetSqlConnection()
        {
            SqlConnectionStringBuilder sqlConnectionStringBuilder = new SqlConnectionStringBuilder();
            sqlConnectionStringBuilder.DataSource = ConfigurationManager.AppSettings["Database:DataSource"];
            sqlConnectionStringBuilder.InitialCatalog = ConfigurationManager.AppSettings["Database:InitialCatalog"];
            sqlConnectionStringBuilder.UserID = ConfigurationManager.AppSettings["Database:UserID"];
            sqlConnectionStringBuilder.Password = ConfigurationManager.AppSettings["Database:Password"];

            return new SqlConnection(sqlConnectionStringBuilder.ToString());
        }

        public static async Task<string> UserCompleteHazard(string userPrincipalName)
        {
            using (SqlConnection sqlConnection = GetSqlConnection())
            {
                await sqlConnection.OpenAsync();

                using (SqlCommand sqlCommand = new SqlCommand("UPDATE [PersonnelDetails] SET [CreatedHazardReport] = 'Yes' WHERE [UserPrincipalName] = @UserPrincipalName", sqlConnection))
                {
                    sqlCommand.Parameters.AddWithValue("@UserPrincipalName", new SqlString(userPrincipalName));

                    sqlCommand.ExecuteNonQuery();

                    //SqlDataReader sqlDataReader = await sqlCommand.ExecuteReaderAsync();
                }
            }

            return null;
        }

        public static async Task<string> UserCompleteSafetyBriefing(string userPrincipalName)
        {
            using (SqlConnection sqlConnection = GetSqlConnection())
            {
                await sqlConnection.OpenAsync();

                using (SqlCommand sqlCommand = new SqlCommand("UPDATE [PersonnelDetails] SET [SafetyBriefing] = 'Yes' WHERE [UserPrincipalName] = @UserPrincipalName", sqlConnection))
                {
                    sqlCommand.Parameters.AddWithValue("@UserPrincipalName", new SqlString(userPrincipalName));

                    sqlCommand.ExecuteNonQuery();

                    //SqlDataReader sqlDataReader = await sqlCommand.ExecuteReaderAsync();
                }
            }

            return null;
        }

        public static async Task<string> UserCompleteRefresherCourse(string userPrincipalName)
        {
            using (SqlConnection sqlConnection = GetSqlConnection())
            {
                await sqlConnection.OpenAsync();

                using (SqlCommand sqlCommand = new SqlCommand("UPDATE [PersonnelDetails] SET [SafetyRefresherCourse] = 'Yes' WHERE [UserPrincipalName] = @UserPrincipalName", sqlConnection))
                {
                    sqlCommand.Parameters.AddWithValue("@UserPrincipalName", new SqlString(userPrincipalName));

                    sqlCommand.ExecuteNonQuery();

                    //SqlDataReader sqlDataReader = await sqlCommand.ExecuteReaderAsync();
                }
            }

            return null;
        }

        public static async Task<string> UserCompletePermitCheck(string userPrincipalName)
        {
            using (SqlConnection sqlConnection = GetSqlConnection())
            {
                await sqlConnection.OpenAsync();

                using (SqlCommand sqlCommand = new SqlCommand("UPDATE [PersonnelDetails] SET [PermitCheck] = 'Yes' WHERE [UserPrincipalName] = @UserPrincipalName", sqlConnection))
                {
                    sqlCommand.Parameters.AddWithValue("@UserPrincipalName", new SqlString(userPrincipalName));

                    sqlCommand.ExecuteNonQuery();

                    //SqlDataReader sqlDataReader = await sqlCommand.ExecuteReaderAsync();
                }
            }

            return null;
        }

        public static async Task<string> UserCompletePPERequired(string userPrincipalName)
        {
            using (SqlConnection sqlConnection = GetSqlConnection())
            {
                await sqlConnection.OpenAsync();

                using (SqlCommand sqlCommand = new SqlCommand("UPDATE [PersonnelDetails] SET [PPERequired] = 'Yes' WHERE [UserPrincipalName] = @UserPrincipalName", sqlConnection))
                {
                    sqlCommand.Parameters.AddWithValue("@UserPrincipalName", new SqlString(userPrincipalName));

                    sqlCommand.ExecuteNonQuery();

                    //SqlDataReader sqlDataReader = await sqlCommand.ExecuteReaderAsync();
                }
            }

            return null;
        }

        public static async Task<string> UserCompleteNotifications(string userPrincipalName)
        {
            using (SqlConnection sqlConnection = GetSqlConnection())
            {
                await sqlConnection.OpenAsync();

                using (SqlCommand sqlCommand = new SqlCommand("UPDATE [PersonnelDetails] SET [Notifications] = 'Yes' WHERE [UserPrincipalName] = @UserPrincipalName", sqlConnection))
                {
                    sqlCommand.Parameters.AddWithValue("@UserPrincipalName", new SqlString(userPrincipalName));

                    sqlCommand.ExecuteNonQuery();

                    //SqlDataReader sqlDataReader = await sqlCommand.ExecuteReaderAsync();
                }
            }

            return null;
        }

        public static async Task<string> UserCompletePublicData(string userPrincipalName)
        {
            using (SqlConnection sqlConnection = GetSqlConnection())
            {
                await sqlConnection.OpenAsync();

                using (SqlCommand sqlCommand = new SqlCommand("UPDATE [PersonnelDetails] SET [PUblicData] = 'Yes' WHERE [UserPrincipalName] = @UserPrincipalName", sqlConnection))
                {
                    sqlCommand.Parameters.AddWithValue("@UserPrincipalName", new SqlString(userPrincipalName));

                    sqlCommand.ExecuteNonQuery();

                    //SqlDataReader sqlDataReader = await sqlCommand.ExecuteReaderAsync();
                }
            }

            return null;
        }


        public static async Task<string> GetWorkOrderLocationAsync(string aWorkOrderNumber)
        {
            using (SqlConnection sqlConnection = GetSqlConnection())
            {
                await sqlConnection.OpenAsync();

                using (SqlCommand sqlCommand = new SqlCommand("SELECT [Location] FROM [WorkOrders] WHERE [WOId] = @WorkOrderNumber", sqlConnection))
                {
                    sqlCommand.Parameters.AddWithValue("@WorkOrderNumber", new SqlString(aWorkOrderNumber));

                    SqlDataReader sqlDataReader = await sqlCommand.ExecuteReaderAsync();

                    if (sqlDataReader.HasRows)
                    {
                        await sqlDataReader.ReadAsync();

                        return sqlDataReader.GetSqlString(0).Value;
                    }
                    else
                    {
                        return null;        
                    }
                }
            }
        }

        public static async Task<string> GetWorkOrderDetailAsync(string aWorkOrderNumber)
        {
            using (SqlConnection sqlConnection = GetSqlConnection())
            {
                await sqlConnection.OpenAsync();

                using (SqlCommand sqlCommand = new SqlCommand("SELECT [Detail] FROM [WorkOrders] WHERE [WOId] = @WorkOrderNumber", sqlConnection))
                {
                    sqlCommand.Parameters.AddWithValue("@WorkOrderNumber", new SqlString(aWorkOrderNumber));

                    SqlDataReader sqlDataReader = await sqlCommand.ExecuteReaderAsync();

                    if (sqlDataReader.HasRows)
                    {
                        await sqlDataReader.ReadAsync();

                        return sqlDataReader.GetSqlString(0).Value;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        public static async Task<string> ReformatString(string location)
        {

            location = System.Text.RegularExpressions.Regex.Replace(location, "[A-Z]", " $0");
            location = System.Text.RegularExpressions.Regex.Replace(location, "[0-9]", " $0");
            return location;
        }

        public static async Task<int> GetSeriousNumber(string workOrderDetail)
        {
            int incidents = -1;

            using (SqlConnection sqlConnection = GetSqlConnection())
            {
                await sqlConnection.OpenAsync();

                using (SqlCommand sqlCommand = new SqlCommand("" +
                    "SELECT count([Reference ID]) FROM [PublicationsExport20170727092349-SeriousInjury] WHERE [Description] LIKE @workOrderDetail", sqlConnection))
                //"UNION ALL " +
                //"SELECT count([Reference ID]) FROM [PublicationsExport20170727092039-PotentiallySeriousOccurrence] WHERE [Description] LIKE @workOrderDetail UNION ALL " +
                //"SELECT count([Reference ID]) FROM [PublicationsExport20170727092602-BreakageOfRopeChainCable] WHERE [Description] LIKE @workOrderDetail UNION ALL " +
                //"SELECT count([Reference ID]) FROM [PublicationsExport20170727092843-Electricity] WHERE [Description] LIKE @workOrderDetail", sqlConnection))
                {
                    sqlCommand.Parameters.AddWithValue("@workOrderDetail", new SqlString("%" + workOrderDetail + "%"));

                    SqlDataReader sqlDataReader = await sqlCommand.ExecuteReaderAsync();

                    if (sqlDataReader.HasRows)
                    {
                        await sqlDataReader.ReadAsync();

                        incidents = (sqlDataReader.GetSqlInt32(0).Value);

                        //incidentCounts.Add("PotentiallySerious", sqlDataReader.GetSqlInt32(0).Value)
                        //incidentCounts.Add("PotentiallySerious", sqlDataReader.GetSqlInt32(1).Value)
                        //incidentCounts.Add("PotentiallySerious", sqlDataReader.GetSqlInt32(2).Value)
                        //incidentCounts.Add("PotentiallySerious", sqlDataReader.GetSqlInt32(3).Value)

                    }

                    return incidents;
                }

            }
        }

        public static async Task<int> GetPotentialNumber(string workOrderDetail)
        {
            int incidents = -1;

            using (SqlConnection sqlConnection = GetSqlConnection())
            {
                await sqlConnection.OpenAsync();

                using (SqlCommand sqlCommand = new SqlCommand("" +
                    "SELECT count([Reference ID]) FROM [PublicationsExport20170727092039-PotentiallySeriousOccurrence] WHERE [Description] LIKE @workOrderDetail", sqlConnection))
                //"UNION ALL " +
                //"SELECT count([Reference ID]) FROM [PublicationsExport20170727092039-PotentiallySeriousOccurrence] WHERE [Description] LIKE @workOrderDetail UNION ALL " +
                //"SELECT count([Reference ID]) FROM [PublicationsExport20170727092602-BreakageOfRopeChainCable] WHERE [Description] LIKE @workOrderDetail UNION ALL " +
                //"SELECT count([Reference ID]) FROM [PublicationsExport20170727092843-Electricity] WHERE [Description] LIKE @workOrderDetail", sqlConnection))
                {
                    sqlCommand.Parameters.AddWithValue("@workOrderDetail", new SqlString("%" + workOrderDetail + "%"));

                    SqlDataReader sqlDataReader = await sqlCommand.ExecuteReaderAsync();

                    if (sqlDataReader.HasRows)
                    {
                        await sqlDataReader.ReadAsync();

                        incidents = (sqlDataReader.GetSqlInt32(0).Value);

                        //incidentCounts.Add("PotentiallySerious", sqlDataReader.GetSqlInt32(0).Value)
                        //incidentCounts.Add("PotentiallySerious", sqlDataReader.GetSqlInt32(1).Value)
                        //incidentCounts.Add("PotentiallySerious", sqlDataReader.GetSqlInt32(2).Value)
                        //incidentCounts.Add("PotentiallySerious", sqlDataReader.GetSqlInt32(3).Value)

                    }

                    return incidents;
                }

            }
        }

        public static async Task<int> GetElectricalNumber(string workOrderDetail)
        {
            int incidents = -1;

            using (SqlConnection sqlConnection = GetSqlConnection())
            {
                await sqlConnection.OpenAsync();

                using (SqlCommand sqlCommand = new SqlCommand("" +
                    "SELECT count([Reference ID]) FROM [PublicationsExport20170727092843-Electricity] WHERE [Description] LIKE @workOrderDetail", sqlConnection))
                //"UNION ALL " +
                //"SELECT count([Reference ID]) FROM [PublicationsExport20170727092039-PotentiallySeriousOccurrence] WHERE [Description] LIKE @workOrderDetail UNION ALL " +
                //"SELECT count([Reference ID]) FROM [PublicationsExport20170727092602-BreakageOfRopeChainCable] WHERE [Description] LIKE @workOrderDetail UNION ALL " +
                //"SELECT count([Reference ID]) FROM [PublicationsExport20170727092843-Electricity] WHERE [Description] LIKE @workOrderDetail", sqlConnection))
                {
                    sqlCommand.Parameters.AddWithValue("@workOrderDetail", new SqlString("%" + workOrderDetail + "%"));

                    SqlDataReader sqlDataReader = await sqlCommand.ExecuteReaderAsync();

                    if (sqlDataReader.HasRows)
                    {
                        await sqlDataReader.ReadAsync();

                        incidents = (sqlDataReader.GetSqlInt32(0).Value);

                        //incidentCounts.Add("PotentiallySerious", sqlDataReader.GetSqlInt32(0).Value)
                        //incidentCounts.Add("PotentiallySerious", sqlDataReader.GetSqlInt32(1).Value)
                        //incidentCounts.Add("PotentiallySerious", sqlDataReader.GetSqlInt32(2).Value)
                        //incidentCounts.Add("PotentiallySerious", sqlDataReader.GetSqlInt32(3).Value)

                    }

                    return incidents;
                }

            }
        }

       
        /*
        public static async Task<List<int>> GetIncidentNumberPotential(string workOrderDetail)
        {

            List<int> incidents = new List<int>();

            using (SqlConnection sqlConnection = GetSqlConnection())
            {
                await sqlConnection.OpenAsync();

                using (SqlCommand sqlCommand = new SqlCommand("" +
                    "SELECT count([Reference ID]) FROM [PublicationsExport20170727092039-PotentiallySeriousOccurrence] WHERE [Description] LIKE @workOrderDetail UNION ALL " +
                    "SELECT count([Reference ID]) FROM [PublicationsExport20170727092349-SeriousInjury] WHERE [Description] LIKE @workOrderDetail UNION ALL " +
                    "SELECT count([Reference ID]) FROM [PublicationsExport20170727092602-BreakageOfRopeChainCable] WHERE [Description] LIKE @workOrderDetail UNION ALL " +
                    "SELECT count([Reference ID]) FROM [PublicationsExport20170727092843-Electricity] WHERE [Description] LIKE @workOrderDetail", sqlConnection))
                {
                    sqlCommand.Parameters.AddWithValue("@workOrderDetail", new SqlString("%"+workOrderDetail+"%"));

                    SqlDataReader sqlDataReader = await sqlCommand.ExecuteReaderAsync();

                    if (sqlDataReader.HasRows)
                    {
                        await sqlDataReader.ReadAsync();
                        C
                        incidents.Add(sqlDataReader.GetSqlInt32(0).Value);
                        incidents.Add(sqlDataReader.GetSqlInt32(1).Value);
                        incidents.Add(sqlDataReader.GetSqlInt32(2).Value);
                        incidents.Add(sqlDataReader.GetSqlInt32(3).Value);


                        //incidentCounts.Add("PotentiallySerious", sqlDataReader.GetSqlInt32(0).Value)
                        //incidentCounts.Add("PotentiallySerious", sqlDataReader.GetSqlInt32(1).Value)
                        //incidentCounts.Add("PotentiallySerious", sqlDataReader.GetSqlInt32(2).Value)
                        //incidentCounts.Add("PotentiallySerious", sqlDataReader.GetSqlInt32(3).Value)


                        

                    }

                    return incidents;
                }
                
            }
        }
        */
        public static async Task<List<string>> GetPermitByWorkOrder(string aWorkOrderNumber, string username)
        {
            List<string> result = new List<string>();

            using (SqlConnection sqlConnection = GetSqlConnection())
            {
                await sqlConnection.OpenAsync();

                using (SqlCommand sqlCommand = new SqlCommand("SELECT [Name] from[Permits] WHERE[PermitID] IN( SELECT [Permits_ID] FROM [WorkOrder_Permits] WHERE [WorkOrder_ID] = @aWorkOrderNumber" +
                    " EXCEPT SELECT [PermitId] FROM UserPermits) group by[Name]", sqlConnection))
                //                using (SqlCommand sqlCommand = new SqlCommand("SELECT [Permits_ID] FROM [WorkOrder_Permits] WHERE [WorkOrder_ID] = @WorkOrderNumber EXCEPT SELECT" +
                //" [PermitId] FROM UserPermits WHERE [UserPrincipalName] = @UserPrincipalName", sqlConnection))
                {
                    sqlCommand.Parameters.AddWithValue("@aWorkOrderNumber", new SqlString(aWorkOrderNumber));
                    //sqlCommand.Parameters.AddWithValue("@UserPrincipalName", new SqlString(username));

                    SqlDataReader sqlDataReader = await sqlCommand.ExecuteReaderAsync();

                    while (await sqlDataReader.ReadAsync())
                    {
                        result.Add(sqlDataReader.GetSqlString(0).Value);
                    }
                }
            }                        
            return result;
        }


        //

        //
        public static async Task<string> GetAllExpiringPermits()
        {
            string aPermitNumber = "None";
            {
                using (SqlConnection sqlConnection = GetSqlConnection())
                {
                    await sqlConnection.OpenAsync();

                    using (SqlCommand sqlCommand = new SqlCommand("SELECT [PermitID] from [UserPermits] WHERE [Expiry] = 'Yes'", sqlConnection))

                    {
                        SqlDataReader sqlDataReader = await sqlCommand.ExecuteReaderAsync();

                        while (await sqlDataReader.ReadAsync())
                        {
                            aPermitNumber = sqlDataReader.GetSqlString(0).Value;
                        }
                    }

                }
                return aPermitNumber;
            }
        }

            public static async Task<List<string>> GetExpiringPermits(string aWorkOrderNumber, string username)
        {
            List<string> result = new List<string>();
            /*
            if (aWorkOrderNumber == "N/A")
            {
                using (SqlConnection sqlConnection = GetSqlConnection())
                {
                    await sqlConnection.OpenAsync();

                    using (SqlCommand sqlCommand = new SqlCommand("SELECT [PermitID] from [UserPermits] WHERE [Expiry] = 'Yes'", sqlConnection))

                    {
                        SqlDataReader sqlDataReader = await sqlCommand.ExecuteReaderAsync();

                        while (await sqlDataReader.ReadAsync())
                        {
                            aWorkOrderNumber = sqlDataReader.GetSqlString(0).Value;                            
                        }
                    }

                }
                return aWorkOrderNumber;
            }
            */

            using (SqlConnection sqlConnection = GetSqlConnection())
            {
                await sqlConnection.OpenAsync();

                using (SqlCommand sqlCommand = new SqlCommand("SELECT [Name] from [Permits] WHERE [PermitID] IN( SELECT [Permits_ID] FROM [WorkOrder_Permits] WHERE [WorkOrder_ID] = @WorkOrderNumber " +
                    " INTERSECT SELECT [PermitId] FROM UserPermits WHERE [Expiry] = 'Yes') group by [Name]", sqlConnection))

                //using (SqlCommand sqlCommand = new SqlCommand("SELECT [Name] from[Permits] WHERE[PermitID] IN( SELECT[Permits_ID] FROM[WorkOrder_Permits] WHERE[WorkOrder_ID] " +
                //   "=@WorkOrderNumber INTERSECT SELECT[PermitId] FROM UserPermits WHERE[Expiry] = 'Yes') group by[Name] Union SELECT[Permits_ID] FROM[WorkOrder_Permits] WHERE[WorkOrder_ID]" +
                //   "=@WorkOrderNumber INTERSECT SELECT[PermitId] FROM UserPermits WHERE[Expiry] = 'Yes' ", sqlConnection))
                //using (SqlCommand sqlCommand = new SqlCommand("SELECT[Permits_ID] FROM[WorkOrder_Permits] WHERE[WorkOrder_ID] = 'W30340'INTERSECT SELECT[PermitId] FROM UserPermits WHERE" +
                //"  [UserPrincipalName] = @username AND[Expiry] = 'Yes'", sqlConnection))
                {
                    sqlCommand.Parameters.AddWithValue("@WorkOrderNumber", new SqlString(aWorkOrderNumber));
                    //sqlCommand.Parameters.AddWithValue("@username", new SqlString(username));

                    SqlDataReader sqlDataReader = await sqlCommand.ExecuteReaderAsync();

                    while (await sqlDataReader.ReadAsync())
                    {
                        result.Add(sqlDataReader.GetSqlString(0).Value);
                    }
                }
            }



            return result;
        }


        public static async Task<List<string>> GetBriefingByLocation(string aLocation)
        {
            return new List<string>() { "The floor is slippery when wet.", "Trucks often pass through this area." };
        }

        public static async Task<List<string>> GetHazardsByLocation(string aLocation)
        {
            List<string> result = new List<string>();

            using (SqlConnection sqlConnection = GetSqlConnection())
            {
                await sqlConnection.OpenAsync();

                using (SqlCommand sqlCommand = new SqlCommand("SELECT [HazardDetail] FROM [HazardReports] WHERE [HazardLocation] = @Location", sqlConnection))
                {
                    sqlCommand.Parameters.AddWithValue("@Location", new SqlString(aLocation));

                    SqlDataReader sqlDataReader = await sqlCommand.ExecuteReaderAsync();

                    while (await sqlDataReader.ReadAsync())
                    {
                        result.Add(sqlDataReader.GetSqlString(0).Value);
                    }
                }
            }

            return result;
        }


        public static async Task<List<string>> GetPPEByLocation(string aLocation)
        {
            List<string> result = new List<string>();

            using (SqlConnection sqlConnection = GetSqlConnection())
            {
                await sqlConnection.OpenAsync();

                using (SqlCommand sqlCommand = new SqlCommand("SELECT [PPE] FROM [PPE_Required] WHERE [Location] = @Location", sqlConnection))
                {
                    sqlCommand.Parameters.AddWithValue("@Location", new SqlString(aLocation));

                    SqlDataReader sqlDataReader = await sqlCommand.ExecuteReaderAsync();

                    while (await sqlDataReader.ReadAsync())
                    {
                        result.Add(sqlDataReader.GetSqlString(0).Value);
                    }
                }
            }

            return result;
        }

        public static async Task<List<string>> GetPPEByWorkOrderNumber(string aWorkOrderNumber)
        {
            List<string> result = new List<string>();

            using (SqlConnection sqlConnection = GetSqlConnection())
            {
                await sqlConnection.OpenAsync();

                using (SqlCommand sqlCommand = new SqlCommand("SELECT [PPE] FROM [PPE_ByWorkOrder] WHERE [WorkOrder] = @aWorkOrderNumber", sqlConnection))
                {
                    sqlCommand.Parameters.AddWithValue("@aWorkOrderNumber", new SqlString(aWorkOrderNumber));

                    SqlDataReader sqlDataReader = await sqlCommand.ExecuteReaderAsync();

                    while (await sqlDataReader.ReadAsync())
                    {
                        result.Add(sqlDataReader.GetSqlString(0).Value);
                    }
                }
            }

            return result;
        }

        public static async Task<List<Notification>> GetNotifications(string aUserPrincipalName)
        {
            List<Notification> result = new List<Notification>();

            using (SqlConnection sqlConnection = GetSqlConnection())
            {
                await sqlConnection.OpenAsync();

                using (SqlCommand sqlCommand = new SqlCommand("SELECT A.[ID], CAST(A.[Datetime] AS DATETIME), A.[UserPrincipalName], A.[NotificationType], A.[IsRead], A.[Content] FROM [Notifications] AS A INNER JOIN [NotificationSubscriptions] AS B ON A.[UserPrincipalName] = B.[UserPrincipalName] AND A.[NotificationType] = B.[NotificationType] AND A.[UserPrincipalName] = 'chris.okeeffe@au.ey.com' AND A.IsRead = 0", sqlConnection))
                //using (SqlCommand sqlCommand = new SqlCommand("SELECT A.[ID], CAST(A.[Datetime] AS DATETIME), A.[UserPrincipalName], A.[NotificationType], A.[IsRead], A.[Content] FROM [Notifications] AS A INNER JOIN [NotificationSubscriptions] AS B ON A.[UserPrincipalName] = B.[UserPrincipalName] AND A.[NotificationType] = B.[NotificationType] AND A.[UserPrincipalName] = @User", sqlConnection))

                {
                    sqlCommand.Parameters.AddWithValue("@User", new SqlString(aUserPrincipalName));

                    SqlDataReader sqlDataReader = await sqlCommand.ExecuteReaderAsync();

                    while (await sqlDataReader.ReadAsync())
                    {
                        Notification notification = new Notification()
                        {
                            ID = sqlDataReader.GetSqlInt32(0).Value,
                            Timestamp = sqlDataReader.GetSqlDateTime(1).Value,
                            User = sqlDataReader.GetSqlString(2).Value,
                            Type = (NotificationType)Enum.Parse(typeof(NotificationType), sqlDataReader.GetSqlString(3).Value),
                            IsRead = sqlDataReader.GetSqlBoolean(4).Value,
                            Content = sqlDataReader.GetSqlString(5).IsNull ? null : sqlDataReader.GetSqlString(5).Value
                        };

                        result.Add(notification);
                    }
                }
            }

            return result;
        }

        public static async System.Threading.Tasks.Task ReadNotification(int aNotificationId)
        {
            using (SqlConnection sqlConnection = GetSqlConnection())
            {
                await sqlConnection.OpenAsync();

                //using (SqlCommand sqlCommand = new SqlCommand("UPDATE [Notifications] SET [IsRead] = 1 WHERE [ID] = @Id", sqlConnection))
                using (SqlCommand sqlCommand = new SqlCommand("UPDATE [Notifications] SET [IsRead] = 1", sqlConnection))
                {
                    //sqlCommand.Parameters.AddWithValue("@Id", new SqlInt32(aNotificationId));

                    SqlDataReader sqlDataReader = await sqlCommand.ExecuteReaderAsync();
                }
            }
        }

        public static async Task<List<NotificationType>> GetNotificationSubscriptions(string aUserPrincipalName)
        {
            List<NotificationType> result = new List<NotificationType>();

            using (SqlConnection sqlConnection = GetSqlConnection())
            {
                await sqlConnection.OpenAsync();

                using (SqlCommand sqlCommand = new SqlCommand("SELECT [NotificationType] FROM [NotificationSubscriptions]", sqlConnection))
                //using (SqlCommand sqlCommand = new SqlCommand("SELECT [NotificationType] FROM [NotificationSubscriptions] WHERE [UserPrincipalName] = @User", sqlConnection))
                {
                    //sqlCommand.Parameters.AddWithValue("@User", new SqlString(aUserPrincipalName));

                    SqlDataReader sqlDataReader = await sqlCommand.ExecuteReaderAsync();

                    while (await sqlDataReader.ReadAsync())
                    {
                        try
                        {
                            result.Add((NotificationType)Enum.Parse(typeof(NotificationType), sqlDataReader.GetSqlString(0).Value));
                        }
                        catch
                        {
                        }
                    }
                }
            }

            result = result.Distinct().ToList();

            return result;
        }

        public static async System.Threading.Tasks.Task SubscribeNotification(string aUserPrincipalName, NotificationType aNotificationType)
        {
            using (SqlConnection sqlConnection = GetSqlConnection())
            {
                await sqlConnection.OpenAsync();

                using (SqlCommand sqlCommand = new SqlCommand("INSERT INTO [NotificationSubscriptions] ([UserPrincipalName], [NotificationType]) VALUES ('chris.okeeffe@au.ey.com', @Type)", sqlConnection))
                //using (SqlCommand sqlCommand = new SqlCommand("INSERT INTO [NotificationSubscriptions] ([UserPrincipalName], [NotificationType]) VALUES (@User, @Type)", sqlConnection))
                {
                    sqlCommand.Parameters.AddWithValue("@User", new SqlString(aUserPrincipalName));
                    sqlCommand.Parameters.AddWithValue("@Type", new SqlString(aNotificationType.ToString()));

                    try
                    {
                        await sqlCommand.ExecuteNonQueryAsync();
                    }
                    catch
                    {
                    }
                }
            }
        }

        public static async System.Threading.Tasks.Task UnsubscribeNotification(string aUserPrincipalName, NotificationType aNotificationType)
        {
            using (SqlConnection sqlConnection = GetSqlConnection())
            {
                await sqlConnection.OpenAsync();

                //using (SqlCommand sqlCommand = new SqlCommand("DELETE FROM [NotificationSubscriptions] WHERE [UserPrincipalName] = @User AND [NotificationType] = @Type", sqlConnection))
                using (SqlCommand sqlCommand = new SqlCommand("DELETE FROM [NotificationSubscriptions] WHERE [NotificationType] = @Type", sqlConnection))
                {
                   // sqlCommand.Parameters.AddWithValue("@User", new SqlString(aUserPrincipalName));
                    sqlCommand.Parameters.AddWithValue("@Type", new SqlString(aNotificationType.ToString()));

                    await sqlCommand.ExecuteNonQueryAsync();
                }
            }
        }

        public static async System.Threading.Tasks.Task ClearNotification(string aNotification )
        {
            using (SqlConnection sqlConnection = GetSqlConnection())
            {
                await sqlConnection.OpenAsync();

                using (SqlCommand sqlCommand = new SqlCommand("UPDATE [Notifications] SET [IsRead] = '1' WHERE [NotificationType] = @aNotification ", sqlConnection))
                {
                    sqlCommand.Parameters.AddWithValue("@aNotification", new SqlString(aNotification.ToString()));
                    await sqlCommand.ExecuteNonQueryAsync();
                }
            }
        }

        public static async System.Threading.Tasks.Task ClearBroadcast(string aNotification, int aBroadcastNumber)
        {
            using (SqlConnection sqlConnection = GetSqlConnection())
            {
                await sqlConnection.OpenAsync();

                using (SqlCommand sqlCommand = new SqlCommand("UPDATE [Notifications] SET [IsRead] = '1' WHERE [NotificationType] = @aNotification AND [ID] = @aBroadcastNumber ", sqlConnection))
                {
                    sqlCommand.Parameters.AddWithValue("@aNotification", new SqlString(aNotification.ToString()));
                    sqlCommand.Parameters.AddWithValue("@aBroadcastNumber", new SqlInt32(aBroadcastNumber));
                    await sqlCommand.ExecuteNonQueryAsync();
                }
            }
        }
    }
}