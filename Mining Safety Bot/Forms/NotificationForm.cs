﻿using Microsoft.Bot.Builder.FormFlow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mining_Safety_Bot
{
    public enum NotificationOperationType { Subscribe = 1, Unsubscribe = 2 }
    public class NotificationForm
    {
        [Prompt("These are the available notification types I can manage for you. {||}")]
        public NotificationType Notification;
        [Prompt("Would you like to **subscribe** or **unsubscribe** from the notification? {||}")]
        public NotificationOperationType NotificationOperation;

        public static IForm<NotificationForm> BuildForm()
        {
            return new FormBuilder<NotificationForm>()
                .Field(nameof(Notification))
                .Field(nameof(NotificationOperation))
                .Build();
        }
    }
}