﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.FormFlow;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace Mining_Safety_Bot
{
    public class SuggestionForm
    {
        [Prompt("What is your suggestion?")]
        public string SuggestionDetail;


        public static IForm<SuggestionForm> BuildForm()
        {
            return new FormBuilder<SuggestionForm>()

                .AddRemainingFields()
                .Build();
        }

    }
}