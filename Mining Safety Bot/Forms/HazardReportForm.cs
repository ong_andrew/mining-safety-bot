﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.FormFlow;
using Microsoft.Bot.Builder.FormFlow.Advanced;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace Mining_Safety_Bot
{
    //public enum HazardType { Fire = 1, Spill, ExposedCable,  LoudNoise, Other };

    public enum LocationType { Crusher3 = 1, MaintenanceWorkshop1, TrainLoadout3, CarDumper2, ScreeningPlant1, HVWorkshop2, Other }


    public class HazardReportForm
    {

        [Prompt("Are you currently at the hazard?")]
        public bool AtHazard;
        [Prompt("When did you observe this Hazard?")]
        public DateTime Occurrence;
        [Prompt("Where is the hazard located?")]
        public LocationType Location;
       // [Prompt("What type of hazard did you observe?")]
        //public HazardType HazardType;
        [Prompt("Please provide a brief description of the hazard")]
        public string HazardDetail;


        public static IForm<HazardReportForm> BuildForm()
        {
            return new FormBuilder<HazardReportForm>()
                .Field(nameof(AtHazard))
                .Field(new FieldReflector<HazardReportForm>(nameof(Occurrence))
                    //.SetValue(,true)
                    .SetActive(state => !state.AtHazard)
                    //.SetValue(DateTime.
                    .SetNext(
                    (value, state) => (state.AtHazard == true)
                    ? new NextStep(new[] { nameof(Location) })
                    : new NextStep()))
                .Field(new FieldReflector<HazardReportForm>(nameof(Location))
                    .SetActive(state => !state.AtHazard)
                    .SetNext(
                    (value, state) => (state.AtHazard == true) 
                    ? new NextStep(new[] { nameof(HazardDetail) })
                    : new NextStep()))
                .Field(nameof(HazardDetail))

                //.AddRemainingFields()
                //.Confirm("Are the details correct? {||}{*}")

                .Confirm(async (state) =>
                    {

                        if (state.Occurrence.ToString("yyyy") == "0001")
                        {
                            state.Occurrence = DateTime.Now;
                            if (state.Location.ToString() == "0")
                            {
                                state.Location = LocationType.Crusher3;
                            }

                            return new PromptAttribute(
                                $"I have auto-filled the hazard location based off your current location. Are the following details correct?\n\n" +
                                $"Occurence: {state.Occurrence.AddHours(8)}\n\n" +
                                $"Location: {Connections.ReformatString(state.Location.ToString()).Result}\n\n" +
                                $"Hazard Detail: {state.HazardDetail}\n\n" + "{||}");
                        }
                        else
                        {
                            return new PromptAttribute(
                                $"Are the following details correct?\n\n" +
                                $"Occurence: {state.Occurrence.AddHours(8)}\n\n" +
                                $"Location: {Connections.ReformatString(state.Location.ToString()).Result}\n\n" +
                                $"Hazard Detail: {state.HazardDetail}\n\n" + "{||}");
                        }
                        //TimeZoneInfo timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("AUS Eastern Standard Time");
                    })

                .Build();
        }

    }
}