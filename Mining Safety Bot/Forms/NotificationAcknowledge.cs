﻿using Microsoft.Bot.Builder.FormFlow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mining_Safety_Bot
{
    public enum NotificationAcknowledgeType { CompleteSection = 1, Cancel = 2 }
    public class NotificationAcknowledge
    {
        [Prompt("{||}")]
        public NotificationType Notification;

        public static IForm<NotificationForm> BuildForm()
        {
            return new FormBuilder<NotificationForm>()
                .Field(nameof(Notification))
                .Field(nameof(NotificationAcknowledge))
                .Build();
        }
    }
}