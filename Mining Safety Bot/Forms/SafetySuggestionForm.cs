﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.FormFlow;
using Microsoft.Bot.Builder.FormFlow.Advanced;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace Mining_Safety_Bot
{

    public class SafetySuggestionForm
    {

        [Prompt("Are you at the same location as the suggestion?")]
        public bool AtHazard;
        [Prompt("Where is the hazard located?")]
        public LocationType Location;
        [Prompt("Is it for your current work order?")]
        public bool CurrentWorkOrder;
        [Prompt("Please provide a  description of your suggestion")]
        public string HazardDetail;


        public static IForm<SafetySuggestionForm> BuildForm()
        {
            return new FormBuilder<SafetySuggestionForm>()
                .Field(nameof(AtHazard))
                .Field(new FieldReflector<SafetySuggestionForm>(nameof(Location))
                    //.SetValue(,true)
                    .SetActive(state => !state.AtHazard)
                    //.SetValue(DateTime.
                    .SetNext(
                    (value, state) => (state.AtHazard == true)
                    ? new NextStep(new[] { nameof(CurrentWorkOrder) })
                    : new NextStep()))
                .Field(new FieldReflector<SafetySuggestionForm>(nameof(CurrentWorkOrder))
                    .SetActive(state => !state.AtHazard)
                    .SetNext(
                    (value, state) => (state.AtHazard == true)
                    ? new NextStep(new[] { nameof(HazardDetail) })
                    : new NextStep()))
                .Field(nameof(HazardDetail))

                //.AddRemainingFields()
                //.Confirm("Are the details correct? {||}{*}")

                .Confirm(async (state) =>
                {

                    if (state.Location.ToString() == "0")
                    {
                        state.Location = LocationType.MaintenanceWorkshop1;
                        if (state.CurrentWorkOrder.ToString() == "0")
                        {
                            state.CurrentWorkOrder = false;
                        }

                        return new PromptAttribute(
                            $"I have auto-filled the Safety Suggestion location based off your current location. Are the following details correct?\n\n" +
                            $"Location: {Connections.ReformatString(state.Location.ToString()).Result}\n\n" +
                            $"Safety Suggestion: {state.HazardDetail}\n\n" + "{||}");
                    }
                    else
                    {
                        return new PromptAttribute(
                            $"Are the following details correct?\n\n" +
                            $"Location: {Connections.ReformatString(state.Location.ToString()).Result}\n\n" +
                            $"Safety Suggestion: {state.HazardDetail}\n\n" + "{||}");
                    }
                    //TimeZoneInfo timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("AUS Eastern Standard Time");
                })

                .Build();
        }

    }
}