﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.FormFlow;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Mining_Safety_Bot
{
    //public enum HazardType { Fire = 1, Spill, ExposedCable, LoudNoise, Other };

    //public enum LocationType { Kitchen = 1, Shed1, MainEntrance, Bathroom, Yard, Garage2, Other }

    public class HazardRequestForm
    {

        [Prompt("Which specific location you would like? If all, select 'no'"),Optional]
        public LocationType Location;
       // [Prompt("Which specific hazard type would you like? If all, select 'no'"), Optional]
        //public HazardType HazardType;

        public static IForm<HazardRequestForm> BuildForm()
        {
            return new FormBuilder<HazardRequestForm>()
                .AddRemainingFields()
                .Build();
        }

    }
}